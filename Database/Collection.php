<?php
namespace Tikwork\Database;

class Collection extends \ArrayIterator
{

    protected $table          = null;
    protected $objects        = [];
    protected $connectionName = null;
    protected $pdo            = null;
    protected $_objectMapping = null;

    public function __construct($_table, $con = 'default')
    {
        $this->connectionName   = $con;
        $this->table            = $_table;
        $this->pdo              = Connection::getConnection($con);
        $this->IteratorPosition = 0;
    }

    public function read()
    {
        return $this->readWhere();
    }

    public function readWhere($_where = null, $_params = null, $_order = null, $_limit = null)
    {
        $query = 'SELECT ' . PHP_EOL .
            '    *' . PHP_EOL .
            'FROM `' . $this->table . '`' . PHP_EOL;

        if (isset($_where)) {
            $query .= ' WHERE ' . $_where;
        }

        $data = Connection::getArray($query, $_params, $_order, $_limit, $this->connectionName, \PDO::FETCH_ASSOC, 2);

        if ($data) {
            foreach ($data as $value) {
                if ($this->_objectMapping !== null) {
                    $obj = new $this->_objectMapping();
                } else {
                    $obj = new Object($this->table);
                }
                $obj->setDataByArray($value);
                $obj->setPersist(true);
                $this->objects[] = $obj;
            }
        }

        return true;
    }

    public function getArray()
    {
        if ($this->count() == 0) {
            return null;
        }

        return $this->objects;
    }

    public function count()
    {
        return count($this->objects);
    }

    function rewind()
    {
        $this->IteratorPosition = 0;
    }

    function current()
    {
        return $this->objects[$this->IteratorPosition];
    }

    function key()
    {
        return $this->IteratorPosition;
    }

    function next()
    {
        ++$this->IteratorPosition;
    }

    function valid()
    {
        return isset($this->objects[$this->IteratorPosition]);
    }

    public function setObjectMapping($class)
    {
        $this->_objectMapping = $class;
    }

    public function add(Object $obj)
    {
        $this->objects[] = $obj;
    }
}
