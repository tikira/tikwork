<?php
namespace Tikwork\Database;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Tikwork\System\Tikwork;
use Tikwork\System\TikworkException;
use \Exception;

class Connection
{
    const EXCEPTION_ERROR_DB_STATEMENT_ERROR = 5000;
    const DEFAULT_CONNECTION                 = 'default';
    /**
     * @var EntityManager[] $databases
     */
    public static $databases = [];

    public static function init($host, $username, $password, $database, $linkname = self::DEFAULT_CONNECTION)
    {
        $_port      = 3306;
        $db_options = [
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8 COLLATE utf8_general_ci',
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION
        ];

        if (count(explode(':', $host)) > 1) {
            $tmp   = explode(':', $host);
            $host = $tmp[0];
            $port = $tmp[1];
        } else {
            $port = 3306;
        }

        try {
            $dsn = sprintf(
                'mysql:dbname=%s;host=%s;port=%s;charset=utf8;',
                $database,
                $host,
                $port
            );
            $con = new \PDO(
                $dsn,
                $username,
                $password,
                $db_options
            );
        } catch (\PDOException $e) {
            throw new TikworkException('Connection failed ' . $e->getMessage(), TikworkException::ERROR_DB_CONNECTION_FAILED, $e);
        }
        self::$databases[$linkname] = $con;
    }

    /**
     * close connection
     *
     * @param string $connectionName Connectionname
     *
     * @return boolean
     */
    public static function unlink($connectionName = self::DEFAULT_CONNECTION)
    {
        if (self::$databases[$connectionName]) {
            unset(self::$databases[$connectionName]);
            return true;
        }
        return false;
    }

    /**
     * Returns 1 Value of Database
     *
     * @param string $query  Query for the Value
     * @param array  $params Query Params
     * @param string $con    Connectionname
     *
     * @return string
     */
    public static function getOne($query, $params = null, $con = self::DEFAULT_CONNECTION)
    {
        $data = self::getArray($query, $params, null, null, $con, \PDO::FETCH_BOTH, 2);
        if (is_array($data) && isset($data[0])) {
            $data = $data[0];
        }

        return ((is_array($data) && isset($data[0])) ? $data[0] : null);

    }

    /**
     * returns a Array from the given Query
     *
     * @param string $_query      Query for Array
     * @param array  $_params     Params for the Query
     * @param string $_order      Fieldnames for ORDER BY Clause
     * @param string $_limit      Value for LIMIT
     * @param string $con         Connectionname
     * @param int    $pdoParams   PDO Connection Params
     * @param int    $traceSource Source to add the Source information
     *
     * @throws Exception
     *
     * @return array
     */
    public static function getArray($_query, $_params = null, $_order = null, $_limit = null, $con = self::DEFAULT_CONNECTION, $pdoParams = \PDO::FETCH_ASSOC, $traceSource = 1)
    {

        $_query = self::detectSource($_query, $traceSource);

        if ($_order) {
            $_query .= ' ORDER BY ' . $_order;
        }
        if ($_limit) {
            $_query .= ' LIMIT ' . $_limit;
        }

        $link = self::getConnection($con);

        try {
            $stmt = $link->prepare($_query);
            $ok   = $stmt->execute($_params);
        } catch (\Exception $e) {
            throw $e;
        }

        if ($link->errorCode() != '00000') {
            $error = $stmt->errorInfo();
            $error = implode(':', $error);
            throw new TikworkException($error, TikworkException::ERROR_DB_STATEMENT_ERROR);
        }

        try {
            $data = $stmt->fetchAll(($pdoParams) ? $pdoParams : \PDO::FETCH_ASSOC);
            $stmt->closeCursor();
        } catch (\PDOException $e) {
            $source = $e->getTrace()[1];
            throw new TikworkException('Cant fetch data from Database: ' . $e->getMessage(), TikworkException::ERROR_DB_STATEMENT_ERROR);
        }
        $stmt = null;

        return $data;
    }

    /**
     * adds source informations to better debug statements
     * in the statement are following informations on to and on end
     * TOP: File: [FILE]:[LINE]
     * BOT: END OF File: [FILE]:[LINE]
     *
     * @param string $stmt        Statement
     * @param int    $traceSource If we dont come from direct source, here are the "caller"
     *
     * @return string $stmt
     */
    public static function detectSource($stmt, $traceSource = 1)
    {
        $tpl        = '/* File: %s:%s */' . PHP_EOL . '%s' . PHP_EOL . '/* END OF File: %s:%s */' . PHP_EOL;
        $trace      = debug_backtrace();
        $source     = $trace[$traceSource];
        $resultStmt = sprintf($tpl, $source['file'], $source['line'], $stmt, $source['file'], $source['line']);

        if (defined('SQL__DEBUG') && SQL__DEBUG) {
            error_log($resultStmt . PHP_EOL, 3, TIKWORK__LOG_DIR . '/sql-debug-' . date('Y-m-d', time()) . '.log');
        }

        return $resultStmt;
    }

    /**
     * returns a Connection
     *
     * @param string $name Name of the Connection
     *
     * @throws TikworkException
     *
     * @return \PDO
     */
    public static function getConnection($name = self::DEFAULT_CONNECTION)
    {
        if (isset(self::$databases[$name])) {
            return self::$databases[$name];
        } else {
            throw new Exception(sprintf('Connection %s not found not initialized', $name), TikworkException::ERROR_DB_CONNECTION_NOT_FOUND);
        }
    }

    /**
     * @param string $_query      Statment
     * @param array  $_params     Data for Statement
     * @param string $con         Connectionname
     * @param int    $pdoParams   PDO Fetch Options
     * @param int    $traceSource Place from where the Statement is (only to show on statements from where)
     *
     * @return array Resultset
     * @throws \Exception
     */
    public static
    function getSingleRow($_query, $_params = null, $con = self::DEFAULT_CONNECTION, $pdoParams = \PDO::FETCH_ASSOC, $traceSource = 1)
    {
        $_query = self::detectSource($_query, $traceSource);

        return Connection::getArray($_query, $_params, null, 1, $con, $pdoParams, $traceSource + 1)[0];
    }

    public static function execute(
        $_query,
        $_params = null,
        $_order = null,
        $_limit = null,
        $con = null,
        $pdoParams = null
    )
    {
        $_query = self::detectSource($_query);
        $con    = (!$con) ? self::DEFAULT_CONNECTION : $con;

        if ($_order) {
            $_query .= ' ORDER BY ' . $_order;
        }
        if ($_limit) {
            $_query .= ' LIMIT ' . $_limit;
        }

        $link = self::getConnection($con);

        if ($link->errorCode() != \PDO::ERR_NONE) {
            $error = $link->errorInfo();
            $error = implode(':', $error);
            throw new TikworkException($error, self::EXCEPTION_ERROR_DB_STATEMENT_ERROR);
        }

        try {
            $stmt = $link->prepare($_query);
            $ok   = $stmt->execute($_params);
        } catch (Exception $e) {
            throw $e;
        }
        // Clear Queries, to prevent the unbuffered Message

        if ($stmt->errorCode() != \PDO::ERR_NONE) {
            $error = $stmt->errorInfo();
            $error = implode(':', $error);
            throw new TikworkException($error, self::EXCEPTION_ERROR_DB_STATEMENT_ERROR);
        }
        $stmt->closeCursor();

        if ($link->errorCode() != \PDO::ERR_NONE) {
            $error = $link->errorInfo();
            $error = implode(':', $error);
            throw new TikworkException($error, self::EXCEPTION_ERROR_DB_STATEMENT_ERROR);
        }

        return $ok;
    }
}
