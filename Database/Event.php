<?php
namespace Tikwork\Database;

class Event
{
    const INTERVAL_DAY           = 'DAY';
    const INTERVAL_YEAR          = 'YEAR';
    const INTERVAL_QUARTER       = 'QUARTER';
    const INTERVAL_MONTH         = 'MONTH';
    const INTERVAL_HOUR          = 'HOUR';
    const INTERVAL_MINUTE        = 'MINUTE';
    const INTERVAL_WEEK          = 'WEEK';
    const INTERVAL_SECOND        = 'SECOND';
    const INTERVAL_YEAR_MONTH    = 'YEAR_MONTH';
    const INTERVAL_DAY_HOUR      = 'DAY_HOUR';
    const INTERVAL_DAY_MINUTE    = 'DAY_MINUTE';
    const INTERVAL_DAY_SECOND    = 'DAY_SECOND';
    const INTERVAL_HOUR_MINUTE   = 'HOUR_MINUTE';
    const INTERVAL_HOUR_SECOND   = 'HOUR_SECOND';
    const INTERVAL_MINUTE_SECOND = 'MINUTE_SECOND';

    const COMPLETION_ENABLE  = 'PRESERVE';
    const COMPLETION_DISABLE = 'NOT PRESERVE';

    const ENABLED        = 'ENABLE';
    const DISABLED       = 'DISABLE';
    const DISABLED_SLAVE = 'DISABLE ON SLAVE';

    private $pdo                  = null;
    private $EVENT_CATALOG        = null;
    private $EVENT_SCHEMA         = null;
    private $EVENT_NAME           = null;
    private $DEFINER              = 'CURRENT_USER';
    private $TIME_ZONE            = null;
    private $EVENT_BODY           = null;
    private $EVENT_DEFINITION     = null;
    private $EVENT_TYPE           = null;
    private $EXECUTE_AT           = null;
    private $INTERVAL_VALUE       = null;
    private $INTERVAL_FIELD       = null;
    private $SQL_MODE             = null;
    private $STARTS               = null;
    private $ENDS                 = null;
    private $STATUS               = null;
    private $ON_COMPLETION        = null;
    private $CREATED              = null;
    private $LAST_ALTERED         = null;
    private $LAST_EXECUTED        = null;
    private $EVENT_COMMENT        = null;
    private $ORIGINATOR           = null;
    private $CHARACTER_SET_CLIENT = null;
    private $COLLATION_CONNECTION = null;
    private $DATABASE_COLLATION   = null;

    public function __construct($_sName, $connectionName = 'default')
    {
        $this->pdo        = Connection::getConnection($connectionName);
        $this->EVENT_NAME = $_sName;
        $this->getDataFromExistsEvent();
    }

    private function getDataFromExistsEvent()
    {
        $event = Connection::getArray('SELECT * FROM `information_schema`.`EVENTS` WHERE EVENT_NAME = :name', array('name' => $this->EVENT_NAME), null, null, null, $this->pdo);

        if (isset($event[0])) {
            $event = $event[0];
        } else {
            return null;
        }

        $this->EVENT_CATALOG        = $event['EVENT_CATALOG'];
        $this->EVENT_SCHEMA         = $event['EVENT_SCHEMA'];
        $this->EVENT_NAME           = $event['EVENT_NAME'];
        $this->DEFINER              = $event['DEFINER'];
        $this->TIME_ZONE            = $event['TIME_ZONE'];
        $this->EVENT_BODY           = $event['EVENT_BODY'];
        $this->EVENT_DEFINITION     = $event['EVENT_DEFINITION'];
        $this->EVENT_TYPE           = $event['EVENT_TYPE'];
        $this->EXECUTE_AT           = $event['EXECUTE_AT'];
        $this->INTERVAL_VALUE       = $event['INTERVAL_VALUE'];
        $this->INTERVAL_FIELD       = $event['INTERVAL_FIELD'];
        $this->SQL_MODE             = $event['SQL_MODE'];
        $this->STARTS               = $event['STARTS'];
        $this->ENDS                 = $event['ENDS'];
        $this->STATUS               = $event['STATUS'];
        $this->ON_COMPLETION        = $event['ON_COMPLETION'];
        $this->CREATED              = $event['CREATED'];
        $this->LAST_ALTERED         = $event['LAST_ALTERED'];
        $this->LAST_EXECUTED        = $event['LAST_EXECUTED'];
        $this->EVENT_COMMENT        = $event['EVENT_COMMENT'];
        $this->ORIGINATOR           = $event['ORIGINATOR'];
        $this->CHARACTER_SET_CLIENT = $event['CHARACTER_SET_CLIENT'];
        $this->COLLATION_CONNECTION = $event['COLLATION_CONNECTION'];
        $this->DATABASE_COLLATION   = $event['DATABASE_COLLATION'];
    }

    public function setBody($_sBody)
    {
        $this->EVENT_BODY = $_sBody;
    }

    public function setCatalog($_sCatalog)
    {
        $this->EVENT_CATALOG = $_sCatalog;
    }

    public function setIntervalValue($_iInterval)
    {
        $this->INTERVAL_VALUE = $_iInterval;
    }

    public function setIntervalField($_sField)
    {
        $this->INTERVAL_FIELD = $_sField;
    }

    public function setStart($_sStart)
    {
        $this->STARTS = $_sStart;
    }

    public function setEnds($_sEnds)
    {
        $this->ENDS = $_sEnds;
    }

    public function setComment($_sComment)
    {
        $this->EVENT_COMMENT = $_sComment;
    }

    public function setExecuteAt($_sAt)
    {
        $this->EXECUTE_AT = $_sAt;
    }

    public function getScheduleForStatement()
    {
        $result = array();
        if ($this->EXECUTE_AT) {
            $result[] = "AT " . $this->EXECUTE_AT;
        }
        if ($this->INTERVAL_VALUE && $this->INTERVAL_FIELD) {
            $result[] = "EVERY " . $this->INTERVAL_VALUE . ' ' . $this->INTERVAL_FIELD;
        }
        if ($this->STARTS) {
            $result[] = "STARTS " . $this->STARTS;
        }
        if ($this->ENDS) {
            $result[] = "ENDS " . $this->ENDS;
        }

        return implode(" ", $result);
    }

    public function save()
    {
        if ($this->eventExists) {
            $this->alterEvent();
        } else {
            $this->createEvent();
        }
    }

    private function alterEvent()
    {
        $query[] = "ALTER";
        $query[] = "DEFINER " . $this->DEFINER;
        $query[] = "EVENT `" . $this->EVENT_NAME . "`";
        $query[] = "ON SCHEDULE";
        $query[] = $this->getScheduleForStatement();
        $query[] = "ON COMPLETION " . $this->ON_COMPLETION;
        $query[] = $this->STATUS;
        $query[] = "COMMENT '" . $this->EVENT_COMMENT . "'";
        $query[] = "DO";
        $query[] = $this->EVENT_BODY . ";";

        return implode(" \r\n", $query);
    }

    private function createEvent()
    {
        $query[] = "CREATE";
        $query[] = "DEFINER = " . $this->DEFINER;
        $query[] = "EVENT `" . $this->EVENT_NAME . "`";
        $query[] = "ON SCHEDULE";
        $query[] = $this->getScheduleForStatement();
        $query[] = "ON COMPLETION " . $this->ON_COMPLETION;
        $query[] = $this->STATUS;
        if ($this->EVENT_COMMENT) {
            $query[] = "COMMENT '" . $this->EVENT_COMMENT . "'";
        }
        $query[] = "DO";
        $query[] = $this->EVENT_BODY . ';';

        return implode(" \r\n", $query);
    }

}

?>
