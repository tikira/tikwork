<?php
namespace Tikwork\Database;

use Tikwork\Database\Connection;

class Object
{
    protected $pdo             = null;
    protected $tableName       = null;
    protected $connectionName  = null;
    protected $primaryKey      = 'id';
    protected $createdOnField  = 'created_on';
    protected $modifiedOnField = 'modified_on';
    protected $deletedOnField  = 'deleted_on';
    protected $persist         = false;
    protected $dirty           = false;

    public function __construct($tableName, $con = 'default')
    {
        $this->connectionName = $con;
        $this->tableName      = $tableName;
    }

    public function delete()
    {
        $deletedOnField = $this->deletedOnField;
        if ($this->persist && isset($this->$deletedOnField)) {
            $this->$deletedOnField = (new \DateTime())->format('Y-m-d H:i:s');
        }

        return $this->update();
    }

    public function update()
    {


        $data   = $this->getArray();
        $fields = $params = array();
        foreach ($data as $key => $value) {
            $fields[]     = PHP_EOL . '    `' . $key . '` = :' . $key;
            $params[$key] = $value;
        }

        $pk = PHP_EOL . '`' . $this->primaryKey . '` = :' . $this->primaryKey;

        $query = sprintf(
            'UPDATE `%s` ' . PHP_EOL .
            'SET %s ' . PHP_EOL .
            'WHERE %s',
            $this->tableName,
            implode(',', $fields),
            $pk
        );

        return Connection::execute($query, $params, null, null, $this->connectionName);
    }

    public function getArray()
    {
        $result = array();
        $data   = (array)$this;
        foreach ($data as $k => $v) {
            if (ord($k{0}) == 0) {
                continue;
            }
            $result[$k] = $v;
        }

        return $result;
    }

    public function read($id)
    {
        return $this->readWhere($this->primaryKey . ' = :' . $this->primaryKey, array($this->primaryKey => $id));
    }

    public function readWhere($where, $params = null, $orderBy = null)
    {
        $_query = sprintf(
            'SELECT ' . PHP_EOL .
            '  * ' . PHP_EOL .
            'FROM `%s` ' . PHP_EOL .
            'WHERE ' . PHP_EOL .
            '%s',
            $this->tableName,
            $where
        );
        $data   = Connection::getArray($_query, $params, $orderBy, '1', $this->connectionName, \PDO::FETCH_ASSOC, 2);
        if (isset($data[0])) {
            $this->setDataByArray($data[0]);
        }

        $found         = (count($data) > 0) ? true : false;
        $this->persist = $found;

        return $found;
    }

    public function setPersist($boolean)
    {
        $this->persist = $boolean;
    }

    public function setDataByArray($data)
    {
        foreach ($data as $k => $v) {
            $this->$k = $v;
        }
    }

    public function save($debug = false, $dontUpdateModified = false)
    {
        $deletedOnField  = $this->deletedOnField;
        $modifiedOnField = $this->modifiedOnField;
        $createdOnField  = $this->createdOnField;
        if (isset($this->$deletedOnField) && $this->$deletedOnField != null) {
            throw new \Exception('this object is logical deleted and cannot be used anymore');
        }

        if ($this->persist && isset($this->$modifiedOnField) && !$dontUpdateModified) {
            $this->$modifiedOnField = (new \DateTime())->format('Y-m-d H:i:s');
        }
        if (!$this->persist && isset($this->$createdOnField)) {
            $this->$createdOnField = (new \DateTime())->format('Y-m-d H:i:s');
        }

        /*if (!$this->isDirty()) {
            return false;
        }*/

        if ($this->getPKValue() && $this->persist) {
            return $this->update();
        } else {


            return $this->insert();
        }
    }

    private function getPKValue()
    {
        $pk = $this->primaryKey;

        return $this->$pk;
    }

    public function insert()
    {

        $fields       = array();
        $params       = array();
        $data         = $this->getArray();
        $placeholders = array();

        foreach ($data as $key => $value) {
            if ($value != null) {
                $fields[]       = '`' . $key . '`';
                $params[$key]   = $value;
                $placeholders[] = ':' . $key;
            }
        }

        $query = sprintf(
            'INSERT INTO %s' . PHP_EOL .
            ' (%s) ' . PHP_EOL .
            'VALUES' . PHP_EOL .
            ' (%s) ',
            $this->tableName,
            implode(',', $fields),
            implode(',', $placeholders));

        $status = Connection::execute($query, $params, null, null, $this->connectionName);
        if ($status) {
            $pk            = $this->primaryKey;
            $this->$pk     = Connection::getConnection($this->connectionName)->lastInsertId();
            $this->persist = true;

            return true;
        } else {
            return false;
        }
    }

    public function isDirty()
    {
        return $this->dirty;
    }

    /**
     * Sets the Primary Key for this Object
     *
     * @param string $key Primary Key Column Name
     *
     * @return boolean
     */
    public function setPrimaryKey($key)
    {
        $this->primaryKey = $key;

        return true;
    }

    public function set($k, $v)
    {
        $this->__set($k, $v);

        return $this;
    }

    public function __set($k, $v)
    {
        if (property_exists($this,$k) && $this->$k != $v) {
            $this->dirty = true;
        }
        $this->$k = $v;
    }

}
