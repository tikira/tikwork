<?php
namespace Tikwork\File;
class File
{
    const MODE_READ      = 'r';
    const MODE_WRITE     = 'a';
    const MODE_READWRITE = 'a+';

    public  $mode        = null;
    private $fileHandler = null;
    public  $fileName    = null;

    public function __construct($file, $mode = self::MODE_WRITE)
    {
        $this->fileName = $file;
        $this->mode     = $mode;

        if ($mode == self::MODE_WRITE) {
            if (!is_file($file) && !is_writeable(dirname($file))) {
                throw new \Exception('Cant create file ' . $file . ' ' . dirname($file) . ' is not writable');
            }
            if (is_file($file) && !is_writeable($file)) {
                throw new \Exception('Cant write to file ' . $file . ' is not writable ');
            }
        }
        if (!file_exists($file)) {
            touch($file);
        }
        $this->fileHandler = fopen($file, $mode);
        register_shutdown_function(array($this, 'close'));
    }

    public function getLine($length = null)
    {
        if ($this->mode == self::MODE_READ || $this->mode == self::MODE_READWRITE) {
            return fgets($this->fileHandler, 1024);
        } else {
            throw new \Exception('cant read line from file ' . $this->fileName . ', file is write only');
        }
    }

    public function write($data)
    {
        if ($this->fileHandler !== null && ($this->mode == self::MODE_WRITE || $this->mode == self::MODE_READWRITE)) {
            fputs($this->fileHandler, $data, strlen($data));
        } else {
            throw new \Exception('cant write to a readonly or not existent file ' . $this->fileName);
        }

    }

    public function close()
    {
        fclose($this->fileHandler);
    }

}