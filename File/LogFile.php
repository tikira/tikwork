<?php
namespace Tikwork\File;
class LogFile extends File
{
    public function __construct($file, $mode = self::MODE_WRITE)
    {
        parent::__construct($file, $mode);
    }

    public function write($data)
    {
        parent::write($data . PHP_EOL);
    }
}