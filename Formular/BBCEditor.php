<?php
/**
 * BBCEditor Class
 *
 * PHP version 5
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */

/**
 * BBCEditor Class
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */
class BBCEditor extends Control
{
    public $formName = null;

    /**
     * Sets the Formular-Name
     *
     * @param string $_name Formular-Name
     *
     * @return void
     */
    public function setFormName($_name)
    {
        $this->formName = $_name;
    }

    /**
     * get HTML Code of this Control
     *
     * @return string
     */
    public function __toString()
    {
        if (!$this->formName) {
            throw new Exception('Cant Initialise "BBCEditor" Control without formName');

            return false;
        }
        $style  = '';
        $output = '
            <center>
                <table cellspacing="0" cellpadding="0" class="default" width="100%">
                    <tr>
                        <td colspan="2" class="rowA" style="border:0px;">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr align="center">
                                    <td align="center">
                                        <table width="100%">
                                            <tr>
                                                <td><input class="button" type="button" name="bbcode-button"
                                                value="[B]" onclick=\'format("b","' . $this->formName . '",
                                                "' . $this->name . '")\' ' . $style . '></td>
                                                <td><input class="button" type="button" name="bbcode-button"
                                                value="[I]" onclick=\'format("i","' . $this->formName . '",
                                                "' . $this->name . '")\' ' . $style . '></td>
                                                <td><input class="button" type="button" name="bbcode-button"
                                                value="[U]" onclick=\'format("u","' . $this->formName . '",
                                                "' . $this->name . '")\' ' . $style . '></td>
                                                <td><input class="button" type="button" name="bbcode-button"
                                                value="[CENTER]" onclick=\'format("center",
                                                "' . $this->formName . '","' . $this->name .
            '")\' ' . $style . '></td>
                                                <td><input class="button" type="button" name="bbcode-button"
                                                value="[IMG]" onclick=\'format("img","' . $this->formName . '
                                                ","' . $this->name . '")\' ' . $style . '></td>
                                                <td><input class="button" type="button" name="bbcode-button"
                                                value="[BR]" onclick=\'format1("br","' . $this->formName . '"
                                                ,"' . $this->name . '")\' ' . $style . '></td>
                                                <td><input class="button" type="button" name="bbcode-button"
                                                value="[URL]" onclick=\'insert_link("' . $this->formName . '
                                                ","' . $this->name . '")\' ' . $style . '></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" style="width:100%;">
                                <tr align="center">
                                    <td style="height:20px;width:9%;"><img class="smiley"
                                    src="gfx/smilies/biggrin.gif" alt=":D" width="15" height="15"
                                    onclick=\'insertOwn("' . $this->formName . '","' . $this->name . '"
                                    ,":D");\'></td>
                                    <td style="height:20px;width:9%;"><img class="smiley"
                                    src="gfx/smilies/evil.gif" alt=":evil:" width="15" height="15"
                                    onclick=\'insertOwn("' . $this->formName . '","' . $this->name . '"
                                    ,":evil:");\'></td>
                                    <td style="height:20px;width:9%;"><img class="smiley"
                                    src="gfx/smilies/baby.gif" alt=":baby:" width="15" height="15"
                                    onclick=\'insertOwn("' . $this->formName . '","' . $this->name . '"
                                    ,":baby:");\'></td>
                                    <td style="height:20px;width:9%;"><img class="smiley"
                                    src="gfx/smilies/rolleyes.gif" alt=":rolleyes:" width="15" height="15"
                                    onclick=\'insertOwn("' . $this->formName . '","' . $this->name . '"
                                    ,":rolleyes:");\'></td>
                                    <td style="height:20px;width:9%;"><img class="smiley"
                                    src="gfx/smilies/wink.gif" alt=";)" width="15" height="15"
                                    onclick=\'insertOwn("' . $this->formName . '","' . $this->name . '"
                                    ,";)");\'></td>
                                    <td style="height:20px;width:9%;"><img class="smiley"
                                    src="gfx/smilies/tongue.gif" alt=":P" width="15" height="15"
                                    onclick=\'insertOwn("' . $this->formName . '","' . $this->name . '"
                                    ,":P");\'></td>
                                    <td style="height:20px;width:9%;"><img class="smiley"
                                    src="gfx/smilies/smile.gif" alt=":)" width="15" height="15"
                                    onclick=\'insertOwn("' . $this->formName . '","' . $this->name . '"
                                    ,":)");\'></td>
                                    <td style="height:20px;width:9%;"><img class="smiley"
                                    src="gfx/smilies/mad.gif" alt=":(" width="15" height="15"
                                    onclick=\'insertOwn("' . $this->formName . '","' . $this->name . '"
                                    ,":(");\'></td>
                                    <td style="height:20px;width:9%;"><img class="smiley"
                                    src="gfx/smilies/happy.gif" alt="=)" width="15" height="15"
                                    onclick=\'insertOwn("' . $this->formName . '","' . $this->name . '"
                                    ,"=)");\'></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="rowA" colspan="2" style="border:0px;">';

        $output .= '<textarea id="' . $this->name . '" name="' . $this->name . '" ';

        if ($this->cssClass) {
            $output .= 'class="' . $this->cssClass . '"';
        }
        if ($this->style) {
            $output .= 'style="' . $this->style . '"';
        }
        if ($this->onClick) {
            $output .= 'onclick="' . $this->onClick . '"';
        }
        if ($this->onChange) {
            $output .= 'onchange="' . $this->onChange . '"';
        }
        if ($this->onDblClick) {
            $output .= 'ondblclick="' . $this->onDblClick . '"';
        }
        $output .= '>' . $this->getValue() . '</textarea>
            </td></tr></table>
        </center>
        <script language="JavaScript" type="text/javascript" src="./js/bbc.js">
            function insertOwn(form,field,what) {
                var myField = $(field);
                if (document.selection) {
                    $(field).focus();
                    sel = document.selection.createRange();
                    sel.text = what;
                } else if (myField.selectionStart || myField.selectionStart == "0") {
                    var startPos = myField.selectionStart;
                    var endPos = myField.selectionEnd;
                    myField.value = myField.value.substring(0, startPos) + what
                    + myField.value.substring(endPos, myField.value.length);
                    } else {
                            myField.value += what;
                    }
                }
            }
        </script>';

        return $output;
    }
}

?>