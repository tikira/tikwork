<?php
namespace Tikwork\Formular;
class Button extends Control
{

    public $value   = null;
    public $handler = null;
    public $caption = null;

    /**
     * Constructor
     *
     * @param string $name Name of this Button
     *
     * @return Button
     */
    public function __construct($name)
    {
        $this->name = $name;
        parent::setCaption($name);
    }

    /**
     * Returns the HTML Code of this Button
     *
     * @return string
     */
    public function getHtml()
    {
        $string = null;
        $string .= '<input';
        if ($this->handler) {
            $string .= ' type="submit"';
        } else {
            $string .= ' type="button"';
        }
        $string .= ' name="' . $this->name . '"';
        $string .= ' id="' . $this->name . '"';

        if ($this->caption) {
            $string .= ' value="' . $this->caption . '"';
        } else {
            $string .= ' value="' . $this->name . '"';
        }
        if ($this->readOnly) {
            $string .= ' disabled';
        }
        if ($this->onClick) {
            $string .= ' onClick="' . $this->onClick . '"';
        }
        if ($this->onDblClick) {
            $string .= ' onDblClick="' . $this->onDblClick . '"';
        }
        if ($this->cssClass) {
            $string .= ' class="' . $this->cssClass . '"';
        }
        if ($this->style) {
            $string .= ' style="' . $this->style . '"';
        }

        return $string . '>';
    }

    /**
     * Sets the Handler if Button Clicked
     *
     * @param mixed $array array('class','handler')
     *
     * @return void
     */
    public function setHandler($array)
    {
        $this->handler = $array;
    }

    /**
     * Returns the Handler of this Button
     *
     * @return array
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * Sets the Caption of this Button
     *
     * @param string $caption String for Title this Button
     *
     * @return void
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    /**
     * controls Function, for Check Content of the Control
     * On Buttons, returns always true
     *
     * @return boolean
     */
    public function check()
    {
        return true;
    }
}

?>