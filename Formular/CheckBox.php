<?php
namespace Tikwork\Formular;
class CheckBox extends Control
{

    public $value   = null;
    public $options = null;

    /**
     * Constructor
     *
     * @param string $name Name of this Control
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
        parent::setCaption($name);
    }

    /**
     * returns the HTML Code of this Control
     *
     * @return string
     */
    public function getHtml()
    {
        $ident = ' id="' . $this->name . '" name="' . $this->name . '"';

        $string = '<input type="checkbox" ';

        $string .= $ident;
        if ($this->readOnly) {
            $string .= ' disabled';
        }
        if ($this->onClick) {
            $string .= ' onClick="' . $this->onClick . '"';
        }
        if ($this->onDblClick) {
            $string .= ' onDblClick="' . $this->onDblClick . '"';
        }
        if ($this->onChange) {
            $string .= ' onChange="' . $this->onChange . '"';
        }
        if ($this->cssClass) {
            $string .= ' class="' . $this->cssClass . '"';
        }
        if ($this->style) {
            $string .= ' style="' . $this->style . '"';
        }
        $string .= '>';

        if (strlen($this->caption) > 0) {
            $string .= '<label for="' . $this->name . '">' . $this->caption . '</label>';
        }

        return $string;
    }

    /**
     * Returns boolean if checked or not
     *
     * @return boolean
     */
    public function getValue()
    {
        if (isset($_POST[$this->name]) && $_POST[$this->name] == 'on') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets Caption of this Control
     *
     * @param string $caption Caption of this Control
     *
     * @return void
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    /**
     * Check Function for this Controls
     * it returns always true
     *
     * @return boolean
     */
    public function check()
    {
        return true;
    }
}

?>