<?php
namespace Tikwork\Formular;
class ColorPicker extends Control
{

    public $name = null;

    /**
     * returns HTML Code of this Control
     *
     * @return string
     */
    public function getHtml()
    {
        $string = '';
        $string .= '<input';
        $string .= ' type="text"';
        $string .= ' id="' . $this->name . '"';
        $string .= ' name="' . $this->name . '"';

        if ($this->getValue()) {
            $string .= ' value="' . $this->getValue() . '"';
        }
        if ($this->readOnly) {
            $string .= ' disabled';
        }
        $string .= ' class="color"';
        $string .= ' style="' . $this->style . '"';
        $string .= '>';

        return $string;
    }

    /**
     * Returns true if Value is Okay, else False
     *
     * @return boolean
     */
    public function check()
    {
        if ($this->required && !$this->value) {
            return false;
        }

        return parent::check();
    }
}

?>