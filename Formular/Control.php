<?php
namespace Tikwork\Formular;
class Control
{
    const DIN_DATETIME = '/^(([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2}))(.([0-9]{2}\:[0-9]{2}\:[0-9]{2})?)$/';
    const SIMPLE_TEXT  = '/^([a-zA-Z0-9 .?\[\]_\-\.\:\,]+)$/';
    const DIGITS       = '/^[\d]{1,}$/';
    const NUMBER       = '/^[0-9]{1,}[,.]?[0-9]{1,}$/';
    const DIN_DATE     = '/^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/';
    const EMAIL        = '/^([_\.0-9a-zA-Z-]+@[0-9a-zA-Z][-0-9a-zA-Z\.]+\.[a-zA-Z]{2,6})*$/';
    const HEX_COLOR    = '/^#?([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/';

    public $style         = '';
    public $value         = null;
    public $allow         = null;
    public $name          = null;
    public $error         = false;
    public $caption       = null;
    public $readOnly      = false;
    public $onClick       = null;
    public $onDblClick    = null;
    public $onChange      = null;
    public $cssClass      = null;
    public $required      = false;
    public $cssErrorStyle = "border:1px solid white;background:red;color:white;";
    public $minLength     = null;
    public $maxLength     = null;

    public function __construct($name, $controlsContainer = null, $default = null, $required = null)
    {
        $this->name = $name;
        if ($default) {
            $this->value = $default;
        }
        if ($required) {
            $this->setRequired($required);
        }
        if ($controlsContainer) {
            $controlsContainer->add($this);
        }
    }

    /**
     * Check for this Control
     *
     * @return boolean
     */
    public function check()
    {
        if ($this->allow != null && $this->allow == Control::DIGITS && !is_numeric($this->value)) {
            $this->error = true;
        }

        if ($this->value && $this->minLength > 0 && strlen($this->value) < $this->minLength) {
            $this->error = true;
        }
        if ($this->value && $this->maxLength > 0 && strlen($this->value) > $this->maxLength) {
            $this->error = true;
        }
        if (strlen(trim($this->value)) == 0 && $this->required) {
            $this->error = true;
        }

        if ($this->allow != null && strlen(trim($this->value)) > 0) {
            $this->error = (preg_match($this->allow, $this->value) ? false : true);
        }

        return $this->error;
    }

    /**
     * Sets Min and Max Length of this Control
     *
     * @param integer $min Minimal Length
     * @param integer $max Maximal Length
     *
     * @return void
     */
    public function setLength($min = null, $max = null)
    {
        if ($min) {
            $this->minLength = $min;
        }
        if ($max) {
            $this->maxLength = $max;
        }
    }

    /**
     * Process the Control and Set the Value
     *
     * @return boolean
     */
    public function process()
    {
        if (isset($_POST[$this->getName()])) {
            $this->value = $_POST[$this->name];
            $this->check();
        }
        if ($this->isError()) {
            $this->style .= $this->cssErrorStyle;
        }

        return $this->isError();
    }

    /**
     * returns if this control has an error or not
     *
     * @return boolean
     */
    public function isError()
    {
        return $this->error;
    }

    /**
     * returns value
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * returns the name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the Name of this Control
     *
     * @param string $name Name of this Control
     *
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Sets the CSS-Style
     *
     * @param string $style CSS-STYLE
     *
     * @return void
     */
    public function setCssStyle($style)
    {
        $this->style = $style;
    }

    /**
     * Sets the Allowed Value (Regex)
     *
     * @param string $allow REGEX!
     *
     * @return void
     */
    public function setAllow($allow)
    {
        $this->allow = $allow;
    }

    /**
     * Sets a CSS-Class
     *
     * @param string $class Classname
     *
     * @return void
     */
    public function setCssClass($class)
    {
        $this->cssClass = $class;
    }

    /**
     * Set if error or not
     *
     * @param boolean $_b Control has error yes/no
     *
     * @return void
     */
    public function setError($_b)
    {
        $this->error = $_b;
    }

    /**
     * sets the Caption
     *
     * @param string $_caption Caption
     *
     * @return void
     */
    public function setCaption($_caption)
    {
        $this->caption = $_caption;
    }

    /**
     * Sets the Javascript OnClick event
     *
     * @param string $action JavaScript Action
     *
     * @return void
     */
    public function setOnClick($action)
    {
        $this->onClick = $action;
    }

    /**
     * Sets the OnChange Javascript Event
     *
     * @param string $_action Javascript Action
     *
     * @return void
     */
    public function setOnChange($_action)
    {
        $this->onChange = $_action;
    }

    /**
     * Sets the isRequired Parameter
     *
     * @param boolean $_b Required false/true (default: true)
     *
     * @return void
     */
    public function setRequired($_b = true)
    {
        $this->required = $_b;
    }

    /**
     * Sets the readOnly Parameter
     *
     * @param boolean $_b Value is Read Only
     *
     * @return void
     */
    public function setReadOnly($_b)
    {
        $this->readOnly = $_b;
    }

    /**
     * Sets the OnDblClick Javascript Event
     *
     * @param string $_action Javascript Action
     *
     * @return void
     */
    public function setOnDblClick($_action)
    {
        $this->onDblClick = $_action;
    }

    /**
     * Sets the Default Value
     *
     * @param string $_value Default Value of this Control
     *
     * @return void
     */
    public function setDefault($_value)
    {
        if (!$this->readOnly || !isset($_POST) || !isset($_POST[$this->getName()])) {
            $this->value = $_value;
        }
    }

    /**
     * Returns the HTML Code of this Control
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getHtml();
    }
}