<?php
namespace Tikwork\Formular;

use Tikwork\Utils\Session;

class Controls
{
    const NO_ORIGINATOR = -1;
    const NO_FORM       = 0;
    const CANCEL        = 25;
    const RELOAD        = 75;
    const ERROR         = 99;
    const OK            = 100;

    const MAX_RELOAD_RETRY     = 3;
    const SESSION_RELOAD_COUNT = '__lastReloadCount__';
    const CRC_NAME             = '__lastCrcFromForms__';

    const BTN_SUBMIT = 'submit';
    const BTN_CANCEL = 'cancel';


    public $controls   = array();
    public $originator = null;
    public $name       = null;

    public static $processedFormulars = array();

    /**
     * Constructor
     *
     * @param string $sFormName Formular Name
     * @param string $sUrl Action Url
     *
     * @return Controls
     */
    public function __construct($sFormName, $sUrl)
    {

        $this->name = $sFormName;
        $this->url  = $sUrl;
        $oForm      = new Form($sFormName, $sUrl);
        $this->add($oForm);
    }

    /**
     * Adds a Control to this Container
     *
     * @param mixed $c Control
     *
     * @return void
     */
    public function add(Control $c)
    {
        $this->controls[$c->getName()] = $c;
    }

    /**
     * Processing the Formular
     * Fetching Values, Validate them
     * and Calls a Button Handler
     *
     * @param boolean $bIgnoreReload formular Reload
     *
     * @return integer
     */
    public function process($bIgnoreReload = false)
    {
        if (isset(Controls::$processedFormulars[$this->name])) {
            return Controls::NO_FORM;
        }
        if (!isset($_POST) || count($_POST) == 0) {
            Session::set(self::CRC_NAME, 'NO_CRC');
            Session::set(self::SESSION_RELOAD_COUNT, 0);
            Session::set(self::CRC_NAME, null);

            return Controls::NO_FORM;
        }

        $result = Controls::OK;
        $objs   = $this->get();

        foreach ($objs as $value) {
            if ($value instanceof Button) {
                if ($value->getHandler() && isset($_POST[$value->getName()])) {
                    $this->originator = $value;
                }
            } else {
                if (method_exists($value, 'process')) {
                    $objRes = $value->process();
                }
            }
        }

        if ($this->originator->getName() == self::BTN_CANCEL) {
            return self::CANCEL;
        }

        if (Session::get(self::CRC_NAME) == md5(serialize($_POST)) && !$bIgnoreReload) {
            $result = Controls::RELOAD;

            return $result;
        }

        Session::set(self::CRC_NAME, md5(serialize($_POST)));

        if (!$this->originator) {
            return Controls::NO_ORIGINATOR;
        }
        if ($this->getHandler()) {
            $handler = $this->getHandler();

            Controls::$processedFormulars[$this->name] = 'processed';

            $result = call_user_func($handler->getHandler(), $handler, $result, $this);
        }

        return $result;
    }

    public function getHandler()
    {
        if ($this->originator) {
            return $this->originator;
        }
        throw new Exception('No Form Handler, Input Disruption ?');
    }

    /**
     * Returns a Control by given Key or All
     *
     * @param string $k Key
     *
     * @return mixed
     */
    public function get($k = null)
    {
        if ($k == null) {
            return $this->controls;
        } else {
            return $this->controls[$k];
        }
    }

    /**
     * Returns a Translated String for Result Code
     *
     * @param integer $_result value of Process return
     *
     * @return string
     */
    public static function debugResult($_result)
    {
        switch ($_result) {
            case self::OK:
                return 'OK';
                break;
            case self::ERROR:
                return 'ERROR';
                break;
            case self::NO_FORM:
                return 'NO FORM';
                break;
            case self::NO_ORIGINATOR:
                return 'NO FORM HANDLER BUT GOT POST DATA!!';
                break;
            case self::CANCEL:
                return 'CANCEL';
                break;
            case self::RELOAD:
                return 'RELOAD';
                break;
            default:
                return $_result;
                break;
        }
    }
}

?>