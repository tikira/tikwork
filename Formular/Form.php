<?php
namespace Tikwork\Formular;
class Form extends Control
{

    public $action = null;

    public function __construct($formName, $formUrl)
    {
        $this->name   = $formName;
        $this->action = $formUrl;
    }

    /**
     * Usually used in Smarty
     * This function Opens the HTML-Form Tag
     *
     * @return string
     */
    public function open()
    {
        $result = '<form action="' . $this->action . '" id="' . $this->name . '" method="post">';

        return $result;
    }

    /**
     * Closes the Form tag
     *
     * @return string
     */
    public function close()
    {
        $result = '</form>';

        return $result;
    }

    /**
     * Unused Function
     *
     * @see Control::check()
     *
     * @return true
     */
    public function check()
    {
        return true;
    }

}