<?php
namespace Tikwork\Formular;
class HiddenField extends Control
{

    public $password = false;
    public $name     = null;

    public function getHtml()
    {
        $string = "";
        $string .= "<input";
        $string .= ' type="hidden"';
        $string .= ' id="' . $this->name . '"';
        $string .= ' name="' . $this->name . '"';
        if ($this->getValue()) {
            $string .= ' value="' . $this->getValue() . '"';
        }
        $string .= ">";

        return $string;
    }

    public function __get($k = null)
    {
        return $this;
    }

    public function check()
    {
        if ($this->required && !$this->value) {
            return false;
        }

        return parent::check();
    }

}

?>