<?php
namespace Tikwork\Formular;
class Radio extends Control
{
    const DIRECTION_VERTICAL   = 'vertical';
    const DIRECTION_HORIZONTAL = 'horizontal';
    public $value     = null;
    public $options   = null;
    public $direction = null;

    public function __construct($name)
    {
        $this->name      = $name;
        $this->direction = self::DIRECTION_HORIZONTAL;
        parent::setCaption($name);
    }

    public function getHtml()
    {
        $ident  = ' id="' . $this->name . '" name="' . $this->name . '"';
        $string = null;
        if ($this->readOnly) {
            $string .= ' disabled';
        }
        if ($this->onClick) {
            $string .= ' onClick="' . $this->onClick . '"';
        }
        if ($this->onDblClick) {
            $string .= ' onDblClick="' . $this->onDblClick . '"';
        }
        if ($this->onChange) {
            $string .= ' onChange="' . $this->onChange . '"';
        }
        if ($this->cssClass) {
            $string .= ' class="' . $this->cssClass . '"';
        }
        if ($this->style) {
            $string .= ' style="' . $this->style . '"';
        }

        if ($this->options) {
            foreach ($this->options as $key => $value) {
                if ($this->value == $key) {
                    $selected = "checked";
                } else {
                    $selected = null;
                }
                $result[] = '<input type="radio" ' . $ident . ' ' . $string . ' value="' . $key . '" ' . $selected . '> ' . $value;
                $selected = null;
            }
        } else {
            throw new Exception('Radio need minimum 1 Option !');
        }
        if ($this->direction == self::DIRECTION_HORIZONTAL) {
            return implode("\r\n", $result);
        } elseif ($this->direction == self::DIRECTION_VERTICAL) {
            return implode("<br />\r\n", $result);
        } else {
            throw new Exception('Direction for Radio isnt valid, use the Predefined Constants');
        }

        return $string;
    }

    public function setDirection($_direction)
    {
        $this->direction = $_direction;
    }

    public function setOptions($_array)
    {
        $this->options = $_array;
    }

    public function setCaption($text)
    {
        $this->caption = $text;
    }

    public function check()
    {
        return true;
    }

}