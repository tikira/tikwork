<?php
namespace Tikwork\Formular;
class SelectField extends Control
{

    public $value   = null;
    public $options = null;

    public function __construct($name)
    {
        $this->name = $name;
        parent::setCaption($name);
    }

    public function getHtml()
    {
        $string = sprintf('<select id="%s" name="%s"', $this->name, $this->name);
        if ($this->readOnly) {
            $string .= ' disabled';
        }
        if ($this->onClick) {
            $string .= ' onClick="' . $this->onClick . '"';
        }
        if ($this->onDblClick) {
            $string .= ' onDblClick="' . $this->onDblClick . '"';
        }
        if ($this->onChange) {
            $string .= ' onChange="' . $this->onChange . '"';
        }
        if ($this->cssClass) {
            $string .= ' class="' . $this->cssClass . '"';
        }
        if ($this->style) {
            $string .= ' style="' . $this->style . '"';
        }
        $string .= '>';
        if ($this->options) {
            foreach ($this->options as $key => $value) {
                $selected = ($this->value == $key) ? 'selected' : '';
                $string .= sprintf('<option value="%s" %s>%s</option>', $key, $selected, $value);
            }
        }
        $string .= '</select>';

        return $string;
    }

    public function setHandler($array)
    {
        $this->handler = $array;
    }

    public function setOptions($_array)
    {
        $this->options = $_array;
    }

    public function getHandler()
    {
        return $this->handler;
    }

    public function setCaption($text)
    {
        $this->caption = $text;
    }

    public function check()
    {
        if (!parent::check()) {
            return false;
        }
        if ($this->value && in_array($this->value, $this->options)) {
            return true;
        }

        return false;
    }

}
