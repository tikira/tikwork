<?php
namespace Tikwork\Formular;
class TextArea extends Control
{

    public $name = null;

    public function getHtml()
    {

        $string = "<textarea";
        $string .= ' id="' . $this->name . '" name="' . $this->name . '"';
        if ($this->readOnly) {
            $string .= ' disabled';
        }
        if ($this->onClick) {
            $string .= ' onClick="' . $this->onClick . '"';
        }
        if ($this->onDblClick) {
            $string .= ' onDblClick="' . $this->onDblClick . '"';
        }
        if ($this->onChange) {
            $string .= ' onChange="' . $this->onChange . '"';
        }
        if ($this->cssClass) {
            $string .= ' class="' . $this->cssClass . '"';
        }
        if ($this->style) {
            $string .= ' style="' . $this->style . '"';
        }
        $string .= ">";
        $string .= $this->getValue();
        $string .= "</textarea>";

        return $string;
    }

    public function __get($k = null)
    {
        return $this;
    }

    public function check()
    {
        if ($this->required && !$this->value) {
            return false;
        }

        return parent::check();
    }

}

?>