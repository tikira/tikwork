<?php
namespace Tikwork\Formular;
class TextBox extends Control
{

    public $password = false;
    public $name     = null;

    public function getHtml()
    {
        $string = "";
        $string .= "<input";
        if ($this->password) {
            $string .= ' type="password"';
        } else {
            $string .= ' type="text"';
        }
        $string .= ' id="' . $this->name . '"';
        $string .= ' name="' . $this->name . '"';
        $string .= ' value="' . $this->getValue() . '"';

        if ($this->readOnly) {
            $string .= ' disabled';
        }
        if ($this->onClick) {
            $string .= ' onClick="' . $this->onClick . '"';
        }
        if ($this->onDblClick) {
            $string .= ' onDblClick="' . $this->onDblClick . '"';
        }
        if ($this->onChange) {
            $string .= ' onChange="' . $this->onChange . '"';
        }
        if ($this->cssClass) {
            $string .= ' class="' . $this->cssClass . '"';
        }
        $string .= ' style="' . $this->style . '"';
        $string .= ">";

        return $string;
    }

    public function isPassword($_b = null)
    {
        if ($_b) {
            $this->password = true;
        } else {
            return $this->password;
        }
    }

    public function __get($k = null)
    {
        return $this;
    }

    public function check()
    {
        if ($this->required && !$this->value) {
            return false;
        }

        return parent::check();
    }

}

?>