<?php
namespace Tikwork\Interfaces;

interface CacheHandler
{
    public function get($key);

    public function set($key, $value);

    public function connect($config);

    public function disconnect();

    public function delete($key);

}