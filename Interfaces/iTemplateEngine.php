<?php
namespace Tikwork\Interfaces;
interface iTemplateEngine
{
    public function assign($var,$value);
    public function fetch($template=null,$cache_id=null,$compile_id=null,$parent=null,$display=false,$merge_tpl_vars=true,$no_output_filter=false);
}