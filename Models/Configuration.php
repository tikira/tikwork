<?php
namespace Tikwork\Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="configuration")
 */
class Configuration
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="string")
     * @var string $configuration_id
     */
    protected $configuration_id = null;

    /**
     * @ORM\Column(name="application",length=100,type="string")
     *
     */
    protected $application = null;

    /**
     * @ORM\Column(name="name",length=50,type="string")
     */
    protected $name = null;

    /**
     * @ORM\Column(name="value",type="string")
     */
    protected $value = null;

    /**
     * @ORM\Column(name="createdon",type="datetime")
     */
    protected $createdon = null;
    /**
     * @ORM\Column(name="changedon",type="datetime")
     */
    protected $changedon = null;

    /**
     * @ORM\Column(name="version",type="integer")
     */
    protected $version = null;
}