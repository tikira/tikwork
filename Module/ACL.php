<?php
namespace Tikwork\Module;
class ACL
{
    private $acl = [];

    public function setAcl($user = null, $group = null, $action = null)
    {
        if ($action) {
            $this->acl[$action] = [
                'user'  => $user,
                'group' => $group
            ];
        }
    }

    public function checkAcl($user = null, $group = null, $action = null)
    {
        if (isset($this->acl[$action])) {
            $acl = $this->acl[$action];
            if ($user) {
                if (is_array($acl['user'])) {
                    foreach ($acl['user'] as $v) {
                        if ($v == $user) {
                            return true;
                        }
                    }
                } else {
                    if (in_array($user, $acl['user'])) {
                        return true;
                    }
                }
            }

            if ($group) {
                if (is_array($acl['groups'])) {
                    foreach ($acl['groups'] as $v) {
                        if ($v == $group) {
                            return true;
                        }
                    }
                } else {
                    if (in_array($group, $acl['group'])) {
                        return true;
                    }
                }
            }
        } else {
            return true;
        }
    }

    public function getAcl()
    {
        return $this->acl;
    }
}