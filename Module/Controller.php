<?php
namespace Tikwork\Module;

use Tikwork\System\Event;

abstract class Controller
{
    /**
     * @var ControllerModel
     */
    public $model         = null;
    public $controllerUrl = null;
    /**
     * @var ACL
     */
    public $oAcl = null;

    abstract public function init();

    public function checkAcl($user = null, $group = null, $action = 'default')
    {
        if ($this->oAcl) {
            return $this->oAcl->checkAcl($user, $group, strtolower($action));
        }
    }

    public function getControllerUrl($action = null, $params = null)
    {
        $url = $this->controllerUrl . $action;
        if ($params) {
            if (is_array($params)) {
                $url .= '?' . http_build_query($params);
            } else {
                if (is_string($params)) {
                    $url .= '?' . $params;
                }
            }
        }

        return $url;
    }

    public function action_index()
    {
        if (method_exists($this, 'getOutput')) {
            return $this->getOutput();
        }
    }

    /**
     * Sets the Pagetitle with Event (APPLICATION:SET_PAGE_TITLE)
     *
     * @param string $title Set Page Title
     */
    public function setPageTitle($title)
    {
        Event::fire('APPLICATION:SET_PAGE_TITLE', $title);
    }

    public static function getController($ctrl, $package)
    {
        if (!defined('CONTROLLER_PATH')) {
            throw new \Exception('no controller path defined. please define Constant: CONTROLLER_PATH', ERROR_CODE_CONTROLLER_UNKNOWN);
        }
        $controllerFile = CONTROLLER_PATH . $package . '/' . $ctrl . '.php';
        if (!is_dir(CONTROLLER_PATH . $package)) {
            throw new \Exception('not a valid controller ' . CONTROLLER_PATH . $package, ERROR_CODE_CONTROLLER_UNKNOWN);
        }
        if (!file_exists($controllerFile)) {
            throw new \Exception('not a valid controller ' . $controllerFile, ERROR_CODE_CONTROLLER_UNKNOWN);
        }
        require_once($controllerFile);
        $ctrlName = $ctrl . 'Controller';

        try {
            $controller                = new $ctrlName;
            $controller->oAcl          = new ACL;
            $controller->controllerUrl = $package . '/' . $ctrl . '/';
            $controller->init();
        } catch (Exception $e) {
            throw new \Exception('Internal Controller Error: ' . $e->getMessage(), ERROR_CODE_CONTROLLER_ERROR);
        }


        return $controller;
    }

    public function makeUrl($action, $params = null)
    {
        $url = $this->controllerUrl . $action;
        if (is_array($params)) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }
}