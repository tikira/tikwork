<?php
namespace Tikwork\REST;

use Slim;

abstract class Action
{
    public  $restMembers = array(
        'id' => array(
            'allowed'  => null,
            'required' => false
        )
    );
    private $data        = array();
    public  $objectId    = null;
    public  $objectType  = null;
    /** @var Slim */
    private $slim = null;

    public function __construct()
    {
        if (!empty($this->members)) {
            $this->members = array_merge($this->restMembers, $this->members);
        } else {
            $this->members = $this->restMembers;
        }
    }

    public function __get($key)
    {
        if (!empty($this->data[$key])) {
            return $this->data[$key];
        }
    }

    public function getData()
    {
        return $this->data;
    }

    public function process()
    {
        $slim    = new Slim();
        $request = $slim->request();
        switch ($request->getMethod()) {
            case 'GET':
                return $this->ActionGet($request->get());
                break;
            case 'POST':
                $data = $request->post();
                if (!$this->validateRESTParams($data)) {
                    return $this->errorData;
                } else {
                    return $this->ActionCreate($this->params);
                }
                break;
            case 'PUT':
                $data = $request->put();

                if (!$this->validateRESTParams($data)) {
                    return $this->errorData;
                } else {
                    return $this->ActionUpdate($this->params);
                }
                break;
            case 'DELETE':
                $this->ActionDelete($request->delete());
                break;
            default:
                throw new Exception('Unknown REST Action: ' . $this->requestType);
        }
    }

    private function validateRESTParams($params)
    {
        $newParams = array();
        $members   = $this->getRESTMembers();

        if (is_array($members)) {
            foreach ($members as $key => $options) {
                $options = (object)$options;

                if (
                    property_exists($options, 'required') &&
                    $options->required == true &&
                    !property_exists($params, $key)
                ) {
                    $this->sendMessage('Missing Required Parameter: ' . $key, false);
                }

                if (
                    property_exists($options, 'allowed') &&
                    $options->allowed != null &&
                    property_exists($params, $key) &&
                    !preg_match($options->allowed, $params->$key)
                ) {
                    $this->sendMessage('Parameter "' . $key . '" is Invalid!', false);
                }
                if (property_exists($params, $key)) {
                    $newParams[$key] = $params->$key;
                }
            }
            $this->params = (object)$newParams;
        }

        return true;
    }

    private function sendMessage($msg, $success = true)
    {
        $result = array(
            'success' => $success,
            'message' => $msg
        );

        return $this->sendData($result);
    }

    private function sendData($_data)
    {
        $_data = $this->convertToUtf8($_data);
        //ob_start('ob_gzhandler');
        echo json_encode($_data);

        //ob_flush();
        return true;
    }

    private function convertToUtf8($_data)
    {
        $data = mb_convert_variables('UTF-8', 'auto', $_data);

        return $_data;
    }


    /**
     * Function gets Called when fetching a List or a Single Object
     *
     * @param Request $request
     * @param Response $response
     *
     * @return array
     */
    abstract public function ActionGet(Request $request, Response $response);

    /**
     * function to create the object
     *
     * @param Request $request
     * @param Response $response
     *
     * @return array
     */
    abstract public function ActionCreate(Request $request, Response $response);

    /**
     * function to delete a object
     *
     * @param Request $request
     * @param Response $response
     *
     * @return array
     */
    abstract public function ActionDelete(Request $request, Response $response);

    /**
     * function to update a object
     *
     * @param Request $request
     * @param Response $response
     *
     * @return array
     */
    abstract public function ActionUpdate(Request $request, Response $response);

    /**
     * function to set Validators for Incomming Data
     *
     * @return array
     *
     */
    abstract public function getRESTMembers();
}