<?php
namespace Tikwork\System;
class Event
{
    const EVENT_PROCESS_HTML_REQUEST = 'process_request_html';
    const EVENT_INIT_FRAMEWORK       = 'init:framework';

    const TEMPLATE_DEBUG_ADD_LISTENER      = 'Event Listener: %s -> %s::%s';
    const TEMPLATE_DEBUG_REMOVE_LISTENERS  = 'all Event Listeners for Event %s removed';
    const TEMPLATE_DEBUG_FIRE_EVENT_HEADER = 'Event Firing: %s';
    const TEMPLATE_DEBUG_FIRE_EVENT        = '[%s] -> %s::%s Status: %b';

    private static $events         = array();
    private static $debug          = false;
    private static $debugSeparator = null;

    public static function setDebug($boolean = true)
    {
        self::$debug = $boolean;
        if (php_sapi_name() !== 'cli') {
            self::$debugSeparator = '<br />';
        } else {
            self::$debugSeparator = PHP_EOL;
        }
    }

    public static function addListener($event, $handler)
    {
        if (self::$debug) {
            echo sprintf(self::TEMPLATE_DEBUG_ADD_LISTENER, $event, $handler[0], $handler[1]) . self::$debugSeparator;
        }
        self::$events[$event][] = $handler;
    }

    public static function removeAllListeners($event)
    {
        if (self::$debug) {
            echo sprintf(self::TEMPLATE_DEBUG_REMOVE_LISTENERS, $event) . self::$debugSeparator;
        }
        self::$events[$event] = null;
    }

    public static function fire($event, &$data = null)
    {
        if (self::$debug) {
            echo sprintf(self::TEMPLATE_DEBUG_FIRE_EVENT_HEADER, $event) . self::$debugSeparator;
        }
        if (isset(self::$events[$event]) && self::$events[$event] != null) {
            foreach (self::$events[$event] as $evt) {
                $state = call_user_func($evt, $data);
                if (self::$debug) {
                    echo sprintf(self::TEMPLATE_DEBUG_FIRE_EVENT, $event, $evt[0], $evt[1], $state) . self::$debugSeparator;
                }
            }
        }
    }
}
