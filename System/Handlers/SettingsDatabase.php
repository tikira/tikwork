<?php
namespace Tikwork\System\Handlers;

use Tikwork\Database\Connection;

class SettingsDatabase extends SettingsHandlerAbstract
{
    private $tableName  = 'settings';
    private $fieldName  = 'name';
    private $fieldValue = 'value';

    public function __construct()
    {

    }

    public function prepare($params = null)
    {
        if (is_array($params)) {
            if (isset($params['table'])) {
                $this->tableName = $params['table'];
            }
            if (isset($params['field_name'])) {
                $this->fieldName = $params['field_name'];
            }
            if (isset($params['field_value'])) {
                $this->fieldValue = $params['field_value'];
            }
        }
    }

    public function get($name)
    {
        return Connection::getOne('SELECT `' . $this->fieldValue . '` FROM `' . $this->tableName . '` WHERE `' . $this->fieldName . '` = :' . $this->fieldName, array($this->fieldName => $name));
    }

    public function set($name, $value)
    {
        $stmt   = 'INSERT INTO `' . $this->tableName . '`
            (`' . $this->fieldName . '`,`' . $this->fieldValue . '`)
            VALUES
            (:' . $this->fieldName . ',:' . $this->fieldValue . ')
            ON DUPLICATE KEY UPDATE
            `' . $this->fieldValue . '` = :' . $this->fieldValue;
        $params = array(
            $this->fieldName  => $name,
            $this->fieldValue => $value
        );

        $result = Connection::execute(
            $stmt,
            $params
        );

        return $result;
    }
}