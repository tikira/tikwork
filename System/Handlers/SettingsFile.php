<?php
namespace Tikwork\System\Handlers;
class SettingsFile extends SettingsHandlerAbstract
{
    private $fileName = null;
    private $data     = array();

    public function __construct()
    {

    }

    public function prepare($fileName)
    {
        if (is_string($fileName)) {
            if (!file_exists($fileName)) {
                try {
                    touch($fileName);
                } catch (\Exception $e) {
                    throw new \Exception('Cant create Settings File ' . $fileName);
                }
            }
            $this->fileName = $fileName;
        }

        $data = (array)json_decode(base64_decode(file_get_contents($fileName)));
        if (is_array($data)) {
            $this->data = $data;
        }
        unset($data);
        register_shutdown_function(array($this, 'shutdown'));
    }

    public function get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        } else {
            return null;
        }
    }

    public function set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function shutdown()
    {
        $status = file_put_contents($this->fileName, base64_encode(json_encode($this->data)));
    }
}