<?php
namespace Tikwork\System\Handlers;
abstract class SettingsHandlerAbstract
{
    abstract public function __construct();

    abstract public function get($name);

    abstract public function set($name, $value);

    abstract public function prepare($params);
}