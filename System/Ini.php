<?php
/**
 * Ini Class
 *
 * PHP version 5
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */

/**
 * Ini Class
 * Autoloader, ErrorHandling
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */
namespace Tikwork;
class Ini
{
    protected $ini = null;

    /**
     * Constructor for INI-File Parser
     *
     * @param string $_iniFile the Ini file
     * @param boolean $processSections Parse Sections
     *
     * @return void
     */
    public function __construct($_iniFile, $processSections = false)
    {
        if (file_exists($_iniFile)) {
            $this->ini = parse_ini_file($_iniFile, $processSections);
        }
    }

    /**
     * Get a Ini Parameter
     *
     * @param mixed $_key Key from Ini
     *
     * @return mixed
     */
    public function get($_key = null)
    {
        if (!$_key) {
            return $this->ini;
        }
        if (isset($this->ini[$_key])) {
            return $this->ini[$_key];
        }

        return null;
    }

}

?>