<?php
/**
 * System-Messages
 *
 * PHP version 5
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */

/**
 * System-Messages
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 *
 */
namespace Tikwork;
class Message
{
    const TYPE_INFO    = 'info';
    const TYPE_ERROR   = 'error';
    const TYPE_WARNING = 'warning';
    const TYPE_OK      = 'ok';

    public static $messageArray = array();

    /**
     * Adds a Message
     *
     * @param string $_type Message Type
     * @param string $_message Message Text
     *
     * @return void
     */
    public static function add($_type, $_message)
    {
        if (!$_type) {
            $_type = self::TYPE_INFO;
        }
        Message::$messageArray[] = array(
            'type'    => $_type,
            'message' => $_message
        );
    }

    /**
     * Fetchs a dict Object from Database and Parse the Content with printf
     *
     * @param string $object ObjectID
     * @param string $data Array of Data Elements
     *
     * @return string
     */
    public static
    function fetch($object, $data = null)
    {
        $object = Dict::get($object);

        return printf($object, $data);
    }

    /**
     * gets all added Messages
     *
     * @return array of Messages
     */
    public static function get()
    {
        return Message::$messageArray;
    }
}