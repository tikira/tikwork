<?php
/**
 * abstract Module Class
 *
 * PHP version 5
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */

/**
 * abstract Module Class
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 *
 */
namespace Tikwork\System;

use Tikwork\Database\DBCon;

abstract class Module
{
    const STATUS_ENABLED = 'ENABLED';

    public $dbModule = null;
    public $moduleId = null;
    public $package  = null;
    public $params   = null;

    public $smarty = null;

    public static $moduleParams = null;
    public static $pagetitle    = null;
    public static $modules      = array();

    /**
     * Default Init for Module
     *
     * @return void
     */
    abstract public
    function init();

    /**
     * Default getOutput for Module
     *
     * @return void
     */
    abstract public
    function getOutput();

    public static
    function factory($modulName, $allowRedirect = true)
    {
        if (!$modulName) {
            throw new \Exception('No Module ID or Module Name');
        }

        $o = (object)DBCon::getSingleRow('SELECT * FROM ' . MODUL_TABLE . ' WHERE name = :name or id = :name OR rewrite_url = :name', array('name' => $modulName));

        if ($o->id && $o->id > 0) {
            if ($o->status != Module::STATUS_ENABLED) {
                throw new Exception('Modul is not Avaiable, please try again later');
            }
            $packagePath = BASE_DIR . '/packages/' . $o->package . '/';
            if ($o->package != '') {
                Event::fire(Tikwork::EVENT_ADD_INCLUDE_PATH, $packagePath);
            }
            if ($o->group_ids) {
                $groups    = explode(',', $o->group_ids);
                $access_ok = false;
                foreach ($groups as $g) {
                    if (is_array(Session::get('groups')) && in_array($g, Session::get('groups'))) {
                        $access_ok = true;
                    }
                }

                if (is_array(Session::get('groups')) && in_array(ADMIN_GROUP, Session::get('groups'))) {
                    $access_ok = true;
                }

                if (!$access_ok) {
                    throw new \Exception('Access Denied to Module: ' . $o->logic);
                }
            }
            try {
                $modul = new \ReflectionClass($o->logic);
            } catch (Exception $e) {
                throw $e;
            }

            try {
                $modul = $modul->newInstance();
            } catch (Exception $e) {
                throw $e;
            }

            $modul->dbModule    = $o;
            $modul->moduleId    = $o->id;
            $modul->package     = $o->package;
            $modul->packagePath = $packagePath;
            $modul->smarty      = new \ExtSmarty();
            if ($o->package != '') {
                $modul->smarty->template_dir = $packagePath . '/templates/';
            }
            if (isset(Module::$moduleParams)) {
                $modul->params = $o->params;
            }
            $modul->init();

            return $modul;
        } else {
            throw new \Exception('Unknown Module: ' . $modulName);
        }
    }

    /**
     * Set Title for Module
     *
     * @param String $_sTitle Title for Module
     *
     * @return void
     */
    public function setPageTitle($_sTitle)
    {
        Core::$sPageTitle = $_sTitle;
    }

    /**
     * returns a Module by Redirect
     *
     * @param string $string redirect String
     *
     * @return Module
     */
    public static function processRedirect($string)
    {
        $params   = explode("::", $string);
        $moduleId = $params[1];
        unset($params[0], $params[1]);

        Module::$moduleParams = $params;

        return Module::factory($moduleId);
    }

}

?>