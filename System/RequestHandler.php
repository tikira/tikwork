<?php
namespace Tikwork\System;

class RequestHandler
{
    public static function handleRequest()
    {
        Event::fire(Tikwork::EVENT_APPLICATION_REQUEST);
    }
}