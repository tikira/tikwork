<?php
namespace Tikwork\System;

use Tikwork\Database\Connection;
use Tikwork\File\LogFile;
use Tikwork\File\File;

class Tikwork
{
    public static $debug      = false;
    public static $ini        = null;
    public static $firstError = false;

    const EVENT_REGISTER_HANDLERS   = 'TIKWORK::INIT_HANDLERS';
    const EVENT_ADD_INCLUDE_PATH    = 'TIKWORK::ADD_INCLUDE_PATH';
    const EVENT_LOAD_CONFIG         = 'TIKWORK::LOAD_CONFIG';
    const EVENT_LOAD_DBCONFIG       = 'TIKWORK::LOAD_DBCONFIG';
    const EVENT_REQUEST             = 'TIKWORK::REQUEST';
    const EVENT_APPLICATION_REQUEST = 'TIKWORK::APPLICATION_REQUEST';
    const EVENT_DBSETUP             = 'TIKWORK::DBSETUP';
    const EVENT_SHOW_ERROR_PAGE     = 'TIKWORK::SHOW_ERROR_PAGE';

    public static function init()
    {
        Tikwork::registerGlobalEvents();
        Event::fire(Tikwork::EVENT_REGISTER_HANDLERS);
        $frameworkPath      = realpath(dirname(__FILE__) . '/../');
        $tikworkIncludePath = $frameworkPath . '/';
        Event::fire(Tikwork::EVENT_ADD_INCLUDE_PATH, $tikworkIncludePath);

        if (defined('TIKWORK__AUTOLOAD_CONFIG') && TIKWORK__AUTOLOAD_CONFIG) {
            Event::fire(Tikwork::EVENT_LOAD_CONFIG);
        }
        if (defined('TIKWORK__AUTOLOAD_DBCONFIG') && TIKWORK__AUTOLOAD_DBCONFIG) {
            Event::fire(Tikwork::EVENT_LOAD_DBCONFIG);
        }
        if (defined('TIKWORK__AUTOSYNC_DBSETUP') && TIKWORK__AUTOSYNC_DBSETUP) {
            Event::fire(Tikwork::EVENT_DBSETUP);
        }
        $defaultLanguage = (defined('DEFAULT_LANGUAGE') && DEFAULT_LANGUAGE) ? DEFAULT_LANGUAGE : 'de';
        Tikwork::eventDetectLanguage($defaultLanguage);
    }


    /**
     * Error Handler
     * write to Logfile
     *
     * @param int $errno Error-Number
     * @param string $errstr Error-Message
     * @param string $errfile Error-File
     * @param string $errline Error-Line
     * @param array $vars Variables
     *
     * @throws \Exception
     * @return boolean
     */
    public static function errorHandler($errno, $errstr, $errfile, $errline, $vars = null)
    {
        $errorTpl = '[%s] (%s) %s#%s: %s' . PHP_EOL;
        self::$firstError = false;
        $err              = sprintf(
            $errorTpl,
            date('d.m.Y H:i:s', time()),
            $errno,
            $errfile,
            $errline,
            $errstr
        );
        getLogger()->addError($err);

        return true;
    }

    /**
     * Exception Handler
     * Sends Emails, Write logfile and Shows Exception
     *
     * @param Exception $_exception Exception
     *
     * @return void
     */
    public static function exceptionHandler($_exception)
    {
        $exceptionTpl =
            'Tikwork Exception Handler %s' . PHP_EOL .
            'Exception (%s): %s ' . PHP_EOL .
            'Location: %s#%s ' . PHP_EOL .
            'Trace: %s' . PHP_EOL .
            'REQUEST-Data: %s' . PHP_EOL;

        $ts               = 'exception-' . date('Y-m-d', time()) . '.log';
        $exceptionMessage =
            sprintf(
                $exceptionTpl,
                date('Y-m-d H:i:s', time()),
                $_exception->getcode(),
                $_exception->getMessage(),
                $_exception->getFile(),
                $_exception->getLine(),
                $_exception->getTraceAsString(),
                var_export($_REQUEST, true)
            );
        getLogger()->addEmergency($exceptionMessage);
        if (defined('TIKWORK__DEBUG') && TIKWORK__DEBUG) {
            if (php_sapi_name() !== 'CLI') {
                echo '<pre>' . $exceptionMessage . '</pre>';
            } else {
                echo $exceptionMessage;
            }
        }
        $eventParams = array('exception' => $_exception, 'message' => $exceptionMessage);
        Event::fire(self::EVENT_SHOW_ERROR_PAGE, $eventParams);
    }

    /**
     * Shutdown handler
     *
     * @return void
     */
    public static function shutdownHandler()
    {
        if (error_get_last()) {
            $error = error_get_last();
            Tikwork::errorHandler($error['type'], $error['message'], $error['file'], $error['line']);
        }
    }

    /**
     * Set Error Handler and Autoloader
     *
     * @return boolean
     */
    public static function setHandlers()
    {
        // Native SPL Loader is even more faster than a selfmade
        spl_autoload_register();

        // If we have some Problems, we use the Old slow debugging autoloader
        if (defined('TIKWORK__ERRORHANDLER') || TIKWORK__ERRORHANDLER) {
            set_exception_handler(array('\\Tikwork\\System\\Tikwork', 'exceptionHandler'));
            set_error_handler(array('\\Tikwork\\System\\Tikwork', 'errorHandler'));
            register_shutdown_function(array('\\Tikwork\\System\\Tikwork', 'shutdownHandler'));
        }

        return true;

    }

    /**
     * Enable Debugging
     *
     * @param boolean $_b true/false
     *
     * @return void
     */
    public static function setDebug($_b = true)
    {
        Tikwork::$debug = $_b;
    }

    /**
     * Adds a Include Path to the Include Pathes
     *
     * @param string $sPath Include Path to add
     *
     * @return bool
     */
    public static function addIncludePath($sPath)
    {
        $sPath = str_replace('//', '/', str_replace("\\", "/", $sPath));

        return set_include_path(get_include_path() . PATH_SEPARATOR . $sPath);
    }

    /**
     * AutoLoad
     * Try to load the Class
     * If not Loaded, fallback autoloader was used
     *
     * @param string $sClass The class Name to Load
     *
     * @return boolean
     */
    public static function autoLoader($sClass)
    {
        if (substr($sClass, 0, 7) == 'Tikwork') {
            $sClass = substr($sClass, 8);
        }
        if (substr(strtolower($sClass), 0, 6) == 'smarty') {
            return false;
        }
        $sClass = str_replace("\\", "/", $sClass);
        @$status = @include($sClass . '.php');
        if (!$status) {
            $trace = debug_backtrace();
            if (isset($trace[1]) && is_array($trace[1])) {
                Tikwork::errorHandler('0', 'invalid autoload call (file not found)', $trace[1]['file'], $trace[1]['line']);
            }

            return false;
        }

        return true;
    }

    public static function registerGlobalEvents()
    {
        Event::addListener(Tikwork::EVENT_REGISTER_HANDLERS, array('\\Tikwork\\System\\Tikwork', 'setHandlers'));
        Event::addListener(Tikwork::EVENT_ADD_INCLUDE_PATH, array('\\Tikwork\\System\\Tikwork', 'addIncludePath'));
        Event::addListener(Tikwork::EVENT_LOAD_CONFIG, array('\\Tikwork\\System\\Tikwork', 'eventLoadConfig'));
        Event::addListener(Tikwork::EVENT_LOAD_DBCONFIG, array('\\Tikwork\\System\\Tikwork', 'eventLoadDbConfig'));
        Event::addListener(Tikwork::EVENT_REQUEST, array('RequestHandler', 'handleRequest'));
        Event::addListener(Tikwork::EVENT_DBSETUP, array('\\Tikwork\\Database\\DBSetup', 'process'));
    }

    public static function eventLoadConfig()
    {
        $configIni = CONFIG_DIR . '/config.ini';
        $config    = parse_ini_file($configIni, true);
        if ($config) {
            foreach ($config as $section => $data) {
                if (!is_array($data)) {
                    $field = strtoupper($section);
                    if (!defined($field)) {
                        define($field, $data);
                    }
                } else {
                    foreach ($data as $name => $value) {
                        $field = strtoupper($section . '_' . $name);
                        if (!defined($field)) {
                            define($field, $value);
                        }
                    }
                }
            }
        }

        return true;
    }

    public static function eventLoadDbConfig()
    {

        $dbIni = CONFIG_DIR . '/db.ini';
        if (!file_exists($dbIni)) {
            throw new \Exception('no etc/db.ini file found!');
        }
        $dbconfig = parse_ini_file($dbIni, true);

        if (!isset($dboptions) || empty($dboptions)) {
            $dboptions = array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
            );
        }
        if ($dbconfig && count($dbconfig) > 0) {
            foreach ($dbconfig as $connection => $config) {
                Connection::init($config['host'], $config['username'], $config['password'], $config['database'], $connection, $dboptions);
            }
        }

        return true;

    }

    public static function eventDetectLanguage($default = null)
    {
        $retVal  = $default;
        $arrInfo = explode(',', trim(getenv('HTTP_ACCEPT_LANGUAGE')));
        $arrInfo = explode(';', $arrInfo[0]);
        $arrInfo = explode('-', $arrInfo[0]);

        return strtoupper($retVal);
    }
}