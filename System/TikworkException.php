<?php
namespace Tikwork\System;
class TikworkException extends \Exception
{
    const ERROR_PERMISSION_DENIED          = 10000;
    const ERROR_DB_CONNECTION_FAILED       = 30000;
    const ERROR_DB_STATEMENT_ERROR         = 31000;
    const ERROR_DB_CONNECTION_NOT_FOUND    = 30001;
    const ERROR_MODULE_NO_DATABASE_HANDLER = 10010;
    const ERROR_TEMPLATE_ENGINE            = 50000;
}