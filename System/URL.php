<?php
namespace Tikwork\System;
class URL
{
    public static function createUrl($controller, $action = 'index', $params = null)
    {
        $url = ((isset($_SERVER['REQUEST_SCHEME'])) ? $_SERVER['REQUEST_SCHEME'] : 'CLI') . '://' . ((isset($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : 'localhost') . '/' . $controller . '/' . $action;
        if (is_array($params)) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }

    public static function get($key)
    {
        if (isset($_GET[$key])) {
            return $_GET[$key];
        } else {
            return null;
        }
    }
}