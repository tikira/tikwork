<?php

class AjaxFilter
{
    public static function getArrayFromString($string)
    {
        $filter = json_decode($string);
        $result = array();
        if (is_array($filter)) {
            foreach ($filter as $f) {
                $f->value = str_replace('*', '%', $f->value);

                switch ($f->type) {
                    case 'string' :
                        $result[$f->field] = $f->value;
                        //$qs .= " AND ".$f['field']." LIKE '%".$f['data']['value']."%'";
                        break;
                    case 'list' :
                        if (strstr($f->value, ',')) {
                            $fi = explode(',', $f->value);
                            //$f['data']['value'] = implode(',',$fi);
                            $result[$f->field] = implode(',', $fi);
                            //$qs .= " AND ".$f['field']." IN (".$f['data']['value'].")";
                        } else {
                            $result[$f->field] = $f->value;
                            //$qs .= " AND ".$f['field']." = '".$f['data']['value']."'";
                        }
                        break;
                    case 'boolean' :
                        $result[$f->field] = ($f->value) ? true : false;
                        //$qs .= " AND ".$f['field']." = ".($f['data']['value']);
                        break;
                    case 'date':
                        switch ($f->comparison) {
                            case 'ne' :
                                $qs .= " AND " . $f->field . " != " . $f['data']['value'];
                                break;
                            case 'eq' :
                                $qs .= " AND " . $f['field'] . " = " . $f['data']['value'];
                                break;
                            case 'lt' :
                                $qs .= " AND " . $f['field'] . " < " . $f['data']['value'];
                                break;
                            case 'gt' :
                                $qs .= " AND " . $f['field'] . " > " . $f['data']['value'];
                                break;
                        }
                    case 'numeric' :
                        switch ($f->comparison) {
                            case 'ne' :
                                $qs .= " AND " . $f['field'] . " != " . $f['data']['value'];
                                break;
                            case 'eq' :
                                $qs .= " AND " . $f['field'] . " = " . $f['data']['value'];
                                break;
                            case 'lt' :
                                $qs .= " AND " . $f['field'] . " < " . $f['data']['value'];
                                break;
                            case 'gt' :
                                $qs .= " AND " . $f['field'] . " > " . $f['data']['value'];
                                break;
                        }
                        break;
                }
            }

        }

        SYS::out($result);

        return $result;
    }

    public static
    function applyFilterFromJSONString(
        $_filter,
        &$statement,
        &$params,
        $prefix = 'axfilter')
    {
        $filter = json_decode($_filter);

        $statementWhere = array();

        if (is_array($filter)) {
            $fn = 0;
            foreach ($filter as $f) {
                $f->value = str_replace('*', '%', $f->value);
                $fn++;
                switch ($f->type) {
                    case 'string' :
                        $statementWhere[]                  = $prefix . '.' . $f->field . ' = :' . $prefix . $f->field . $fn;
                        $params[$prefix . $f->field . $fn] = $f->value;
                        break;
                    case 'list' :
                        if (strstr($f->value, ',')) {
                            $fi = explode(',', $f->value);
                            //$f['data']['value'] = implode(',',$fi);
                            $f->value = implode(',', $fi);
                            //$qs .= " AND ".$f['field']." IN (".$f['data']['value'].")";
                        }
                        $statementWhere[] = $prefix . '.' . $f->field . ' IN(' . $f->value . ')';
                        break;
                    case 'boolean' :
                        $statementWhere[]                  = $prefix . '.' . $f->field . ' = :' . $prefix . $f->field . $fn;
                        $params[$prefix . $f->field . $fn] = ($f->value) ? true : false;
                        //$qs .= " AND ".$f['field']." = ".($f['data']['value']);
                        break;
                    case 'numeric':
                    case 'date':
                        switch ($f->comparison) {
                            case 'ne' :
                                $statementWhere[]                  = $prefix . '.' . $f->field . ' != :' . $prefix . $f->field . $fn;
                                $params[$prefix . $f->field . $fn] = $f->value;

                                //$qs .= " AND ".$f->field." != ".$f['data']['value'];
                                break;
                            case 'eq' :
                                $statementWhere[]                  = $prefix . '.' . $f->field . ' = :' . $prefix . $f->field . $fn;
                                $params[$prefix . $f->field . $fn] = $f->value;

                                //$qs .= " AND ".$f['field']." = ".$f['data']['value'];
                                break;
                            case 'lt' :
                                $statementWhere[]                  = $prefix . '.' . $f->field . ' < :' . $prefix . $f->field . $fn;
                                $params[$prefix . $f->field . $fn] = $f->value;

                                //$qs .= " AND ".$f['field']." < ".$f['data']['value'];
                                break;
                            case 'gt' :
                                $statementWhere[]                  = $prefix . '.' . $f->field . ' > :' . $prefix . $f->field . $fn;
                                $params[$prefix . $f->field . $fn] = $f->value;

                                //$qs .= " AND ".$f['field']." > ".$f['data']['value'];
                                break;
                        }
                        break;
                }
            }
            if (count($statementWhere) > 0) {
                $statement = ' SELECT ' . $prefix . '.* FROM (' . $statement . ') ' . $prefix . ' ';
                $statement .= ' WHERE (' . implode(' AND ', $statementWhere) . ') ';
            }
        }

    }

    public static
    function applySortFromJSON(
        &$statement,
        $_sort,
        $prefix = 'axsort')
    {
        $sort = json_decode($_sort);

        $statementSort = array();

        if (is_array($sort)) {
            foreach ($sort as $f) {
                if ($f->direction != 'ASC' && $f->direction != 'DESC') {
                    continue;
                }

                $statementSort[] = $f->property . ' ' . $f->direction;
            }
            if (count($statementSort) > 0) {
                $statement = ' SELECT ' . $prefix . '.* FROM (' . $statement . ') ' . $prefix . ' ';
                $statement .= ' ORDER BY ' . implode(',', $statementSort) . ' ';
            }
        }

    }

    public static function _getArrayFromString($string)
    {
        if ($string == null) {
            return null;
        }
        try {
            $filterArray = (array)json_decode($string);
        } catch (Exception $e) {
            throw $e;
        }

        foreach ($filterArray as $k) {
            $result[$k->field] = $k->value;
        }


        return $result;
    }
}