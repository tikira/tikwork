<?php
namespace Tikwork\Utils;
abstract class Binary
{

    protected $_argv = null;

    abstract public function getArgv();

    public $applicationName = 'no name given';
    public $applicationDesc = 'no application description given';

    public function __construct()
    {
        if ($this->applicationName) {
            echo $this->applicationName . PHP_EOL;
        }
        $this->_argv = $this->getArgv();
        $this->parseArgv();

        foreach ($_SERVER['argv'] as $param) {
            if ($param == '--help') {
                $this->showHelp();
                exit();
            }
        }

        $missingRequired = [];
        foreach ($this->_argv as $param) {
            if ($param->isRequired() && !$param->getValue()) {
                $missingRequired[] = $param;
            }
        }

        if (count($missingRequired) > 0) {
            echo PHP_EOL . 'Missing required Parameters: ' . PHP_EOL . PHP_EOL;
            foreach ($missingRequired as $param) {
                echo '  ' . $param->getUsage() . PHP_EOL;
            }
            echo PHP_EOL . 'end of missing parameters' . PHP_EOL;
            exit();
        }

    }

    public function showHelp()
    {
        echo 'Generic Help for ' . $this->applicationName . PHP_EOL . $this->applicationDesc . PHP_EOL . PHP_EOL;
        foreach ($this->_argv as $param) {
            echo $param->getHelp();
        }
        echo 'Help end' . PHP_EOL;
    }

    public function getParameter($name)
    {
        if (isset($this->_argv[$name])) {
            return $this->_argv[$name]->getValue();
        }
    }

    private function parseArgv()
    {
        $help = ['help' => [
            'required' => false,
            'usage'    => '',
            'help'     => 'Generic Help for Binary'
        ]];

        $this->_argv = array_merge($help, $this->_argv);
        if (is_array($this->_argv)) {
            foreach ($this->_argv as $param => &$value) {
                $value = new TikworkArgVParam($param, $value);
            }
        }
    }

    public function out($message, $color = null)
    {
        SYS::consoleOut($message, $color);
    }
}

class TikworkArgVParam
{
    private $default, $allowed, $required = false, $help, $usage, $formatter, $name, $value = null;

    public function __construct($name, $definition)
    {
        $this->name = $name;
        foreach ($definition as $key => $value) {
            switch ($key) {
                case 'default':
                    $this->default = $value;
                    break;
                case 'allowed':
                    $this->allowed = $value;
                    break;
                case 'required':
                    $this->required = $value;
                    break;
                case 'help':
                    $this->help = $value;
                    break;
                case 'usage':
                    $this->usage = $value;
                    break;
                case 'formatter':
                    $this->formatter = $value;
                    break;
            }
        }

        $data = $_SERVER['argv'];
        foreach ($data as $v) {
            if (!preg_match('/\-\-' . $this->name . '\=(.*)/', $v)) {
                continue;
            }
            $this->value = str_replace('--' . $this->name . '=', '', $v);
            if ($this->allowed && !preg_match($this->allowed, $this->value)) {
                $this->value = false;
                continue;
            }
            if ($this->formatter) {
                $formatter   = $this->formatter;
                $this->value = $formatter($this->value);
            }
        }
    }

    public function getValue()
    {
        return $this->value;
    }

    public function isRequired()
    {
        return $this->required;
    }

    public function getUsage()
    {
        return '--' . $this->name . '=' . $this->usage;
    }

    public function getHelp()
    {
        if ($this->name == 'help') {
            return null;
        }

        return '  ' . str_pad($this->getUsage(), 50, ' ', STR_PAD_RIGHT) . $this->help . PHP_EOL;
    }

}