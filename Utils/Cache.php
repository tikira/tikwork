<?php
/**
 * Cache Class
 *
 * PHP version 5
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */

/**
 * Cache Class
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */
namespace Tikwork\Utils;

use Tikwork\Utils\CacheHandler\CacheHandlerAPC;

class Cache
{
    const TYPE_MEMCACHE = 'memcache';
    const TYPE_SESSION  = 'session';
    const TYPE_APC      = 'apc';

    /**
     * @var CacheHandlerAPC
     */
    public static $oHandler = null;
    public static $use      = null;

    public static function init($type = self::TYPE_APC, $config = null)
    {
        switch ($type) {
            case self::TYPE_SESSION:
                self::$oHandler = new CacheHandlerSession();
                break;
            default:
            case self::TYPE_APC:
                self::$oHandler = new CacheHandlerAPC();
                break;
        }

        return (self::$oHandler !== null);
    }

    /**
     * get data from Cache
     *
     * @param string $_key Key from Cache
     *
     * @return array
     */
    public static function get($key)
    {
        return self::$oHandler->get($key);
    }

    /**
     * Set Data for Key
     *
     * @param string $key Key for Data
     * @param mixed $value Data
     * @param int $ttl timeout (default 1 day)
     *
     * @return boolean
     */
    public static function set($key, $value, $ttl = null)
    {
        return self::$oHandler->set($key, $value, $ttl);
    }

    /**
     * Delete Key
     *
     * @param string $key Key for Data
     *
     * @return boolean
     */
    public static function delete($key)
    {
        return self::$oHandler->set($key);
    }
}