<?php
namespace Tikwork\Utils\CacheHandler;

use Tikwork\Interfaces\CacheHandler;

class CacheHandlerAPC implements CacheHandler
{
    const TTL = 86400;

    public function get($key)
    {

        $success = false;
        $result  = null;
        if (apc_exists($key)) {
            $result = apc_fetch($key, $success);
        }
        if ($success) {
            return $result;
        } else {
            throw new \Exception('Cant fetch Data from APC-Cache');
        }
    }

    public function set($key, $value, $ttl = self::TTL)
    {
        return apc_store($key, $value, $ttl);
    }

    public function connect($config)
    {
        return true;
    }

    public function disconnect()
    {
        return true;
    }

    public function delete($key)
    {
        return apc_delete($key);
    }
}