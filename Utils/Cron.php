<?php
/**
 * Cron Class is maked to run Cronjobs
 * A Cronjob must extend the CronJob Class and define some Parameters to are Valid
 *
 * Class Cron
 */
namespace Tikwork\Utils;
if (!function_exists('posix_getpgid')) {
    throw new \Exception('Cant find Posix_getpgid');
}

class Cron
{
    const CRON_WELCOME                      = 'Tikwork: CronJob Runner v1.0';
    const CRON_MESSAGE_INCLUDE_ERROR        = 'Cron: Cant run Cronjob "%s" reason: %s';
    const CRON_MESSAGE_START                = 'Cron: "%s" is Starting';
    const CRON_MESSAGE_EXIT_FAIL            = 'Cron: "%s" has not well exited.';
    const CRON_MESSAGE_SUCCESS              = 'Cron: "%s" is done %s ms';
    const CRON_MESSAGE_NOT_READY            = 'Cron: "%s" is not ready for processing.exit';
    const CRON_MESSAGE_CLASS_IS_NOT_CRONJOB = 'Cron: "%s" dont extends class "CronJob". Not running. exit.';
    const CRON_MESSAGE_NOT_EXIST            = 'Cron: "%s" not exist';
    const CRON_MESSAGE_TIMEOUT              = 'Cron: Cronjob Timeout from %s sec is reached.';
    const CRON_MESSAGE_PATH_NOT_FOUND       = 'Cron: Constant "CRON_TASK_PATH" not found.exit';
    const CRON_MESSAGE_FORCE_RUN            = 'Cron: Forced to Run the Job!';

    const ARGV_FORCE_RUN = 'force_run';

    // For CronMail
    const STATUS_ERROR      = 'error';
    const STATUS_ERROR_DESC = 'Abbruch';

    const STATUS_OK      = 'success';
    const STATUS_OK_DESC = 'Ok';

    const TIMEOUT = 60;

    private static $messages = array();
    private static $error    = array();
    /**
     * @var CronMail
     */
    private static $mailer = null;
    /**
     * @var CronLog
     */
    private static $log = null;

    public static function out($msg)
    {
        $args = func_get_args();

        unset($args[0]);



        $seperator = (isset($_SERVER['HTTP_HOST'])) ? '<br />' : PHP_EOL;
        $message   = sprintf('[%s]: %s %s', DateX::convert2GER(time(), true), vsprintf($msg, $args), $seperator);
        getLogger()->addInfo($message);
        self::$messages[] = $message;
    }

    public static function runCron($cron)
    {
        self::$mailer = new CronMail(CRONMAIL__RECEIVERS, CRONMAIL__LEVEL);
        self::$log = new CronLog();
        $forceRun     = false;
        if (isset($_SERVER['argv']) && is_array($_SERVER['argv'])) {
            if (in_array(self::ARGV_FORCE_RUN, $_SERVER['argv'])) {
                self::$log->log('running cron with force');
                Cron::out(self::CRON_MESSAGE_FORCE_RUN);
                $forceRun = true;
            }
        }

        if (!defined('CRON_TASK_PATH') || !is_dir(CRON_TASK_PATH)) {
            Cron::out(self::CRON_MESSAGE_PATH_NOT_FOUND);

            return false;
        }
        try {
            $file = CRON_TASK_PATH . '/' . $cron . '.php';
            if (file_exists($file) && is_readable($file)) {
                try {
                    $cronJob = include($file);
                } catch (\Exception $e) {
                    Cron::addError(sprintf(self::CRON_MESSAGE_INCLUDE_ERROR, $cron, $e->getMessage()),true);
                    exit();
                }
                if ($cronJob && $cronJob instanceof CronJob) {
                    $cronJob->sanityCheck($forceRun);
                    if ($cronJob->readyForRun === true) {
                        Settings::set($cronJob->cronRunning, DateX::convert2DIN(time()));
                        Cron::out(sprintf(self::CRON_MESSAGE_START, $cronJob->cronName));
                        $start    = microtime(true);
                        $result   = $cronJob->process();
                        $duration = microtime(true) - $start;
                        if (!$result) {
                            Cron::out(sprintf(self::CRON_MESSAGE_EXIT_FAIL, $cronJob->cronName));
                        } else {
                            Cron::out(sprintf(self::CRON_MESSAGE_SUCCESS, $cronJob->cronName, $duration));
                        }
                        Settings::set($cronJob->cronRunning, '0');
                        Settings::set($cronJob->cronRunning . '_pid', 0);
                        Settings::set($cronJob->cronRunning . '_last_run', DateX::convert2DIN(time()));
                    } else {
                        Cron::out(sprintf(self::CRON_MESSAGE_NOT_READY, $cronJob->cronName));
                        Cron::cronStop($cronJob, self::STATUS_ERROR);
                    }
                } else {
                    Cron::out(sprintf(self::CRON_MESSAGE_CLASS_IS_NOT_CRONJOB, get_class($cronJob)));
                    Cron::cronStop($cronJob, self::STATUS_ERROR);
                }
            } else {
                Cron::out(sprintf(self::CRON_MESSAGE_NOT_EXIST, $cron));
            }
        } catch (Exception $e) {
            Cron::out($e->getMessage());
            Cron::cronStop($cronJob, self::STATUS_ERROR);
        }
        Cron::cronStop($cronJob, self::STATUS_OK);
    }

    public static function shutdown(CronJob $job)
    {
        Settings::set($job->cronRunning, '0');
        Cron::out(sprintf(self::CRON_MESSAGE_TIMEOUT, Cron::TIMEOUT));
        Cron::cronStop($job, self::STATUS_ERROR);
    }

    public static function cronStop(CronJob $cronJob, $status)
    {
        self::$log->log('CRON-STATUS: '.$status.' errors:');
        self::$log->log(implode(PHP_EOL,self::$error));
        $messages = self::$messages;
        if (count(self::$error) > 0) {
            $status   = self::STATUS_ERROR;
            $error    = array();
            $error[]  = '###################################' . PHP_EOL;
            $error[]  = '######### Cronjob Fehler ##########' . PHP_EOL;
            $error[]  = '###################################' . PHP_EOL;
            $error    = array_merge($error, self::$error);
            $error[]  = PHP_EOL . '###################################' . PHP_EOL;
            $error[]  = '######### Logdaten ################ ' . PHP_EOL;
            $error[]  = '###################################' . PHP_EOL;
            $error[]  = '';
            $messages = array_merge($error, self::$messages);
        }
        $sendMessage = implode('', $messages);
        Cron::$mailer->send($cronJob, $sendMessage, $status);
        exit();
    }

    public static function checkPidIsRunning($pid)
    {
        return (posix_getpgid($pid)) ? true : false;
    }

    public static function addError($msg, $autoShow = false)
    {
        self::$error[] = $msg . PHP_EOL;
        if ($autoShow) {
            self::out($msg);
        }
    }
}
