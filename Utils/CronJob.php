<?php
namespace Tikwork\Utils;

use Tikwork\Utils\Settings;

abstract class CronJob
{
    const CRONJOB_MESSAGE_MISSING_CRONNAME      = 'Class "%s" missing variable "cronName". exit.';
    const CRONJOB_MESSAGE_MISSING_CRONENABLED   = 'Class "%s" missing variable "cronEnabled". exit.';
    const CRONJOB_MESSAGE_MISSING_CRONRUN       = 'Class "%s" missing variable "cronRunning". exit.';
    const CRONJOB_MESSAGE_SETTING_ENABLED       = '"%s" is not Enabled. Set the value of "%s to 1 for enabling this Cron. exit.';
    const CRONJOB_MESSAGE_PID_EXIST             = '"%s" has active PID: "%s" dont run this Cron now. exit.';
    const CRONJOB_MESSAGE_CHECK_PID             = 'Checking Pid for %s';
    const CRONJOB_MESSAGE_FOUND_PID             = 'Found PID in Database: "%s"';
    const CRONJOB_MESSAGE_PID_RUNNING           = 'PID "%s" is running.';
    const CRONJOB_MESSAGE_CLEARING_DATABASE_PID = 'PID: "%s" is not running, cleaning up and Notify.';


    public $cronName    = null;
    public $cronEnabled = null;
    public $cronRunning = null;
    public $readyForRun = false;

    public function __construct()
    {

    }

    public function sanityCheck($forceRun)
    {
        if (!$this->cronName) {
            Cron::out(sprintf(self::CRONJOB_MESSAGE_MISSING_CRONNAME, __CLASS__));

            return false;
        }
        if (!$this->cronEnabled) {
            Cron::out(sprintf(self::CRONJOB_MESSAGE_MISSING_CRONENABLED, __CLASS__));

            return false;
        }
        if (!$this->cronRunning) {
            Cron::out(sprintf(self::CRONJOB_MESSAGE_MISSING_CRONRUN, __CLASS__));

            return false;
        }
        if (Settings::get($this->cronEnabled) != '1') {
            if ($forceRun) {
                Settings::set($this->cronEnabled, 1);
            } else {
                Cron::out(sprintf(self::CRONJOB_MESSAGE_SETTING_ENABLED, $this->cronName, $this->cronEnabled));

                return false;
            }
        }

        if (!$this->processPID()) {
            Cron::out(sprintf(self::CRONJOB_MESSAGE_PID_EXIST, $this->cronName, Settings::get($this->cronRunning . '_pid')));

            return false;
        }

        if ($forceRun && method_exists($this, 'install')) {
            Cron::out('Installing Cronjob ' . $this->cronName);
            $this->install();
        }
        $this->readyForRun = true;

        return true;
    }

    public function processPID()
    {
        Cron::out(sprintf(self::CRONJOB_MESSAGE_CHECK_PID, $this->cronName));
        $pidkey = $this->cronRunning . '_pid';
        if (Settings::get($pidkey) > 0) {
            $pid = Settings::get($pidkey);
            Cron::out(sprintf(self::CRONJOB_MESSAGE_FOUND_PID, $pid));
            if (Cron::checkPidIsRunning($pid)) {
                Cron::out(sprintf(self::CRONJOB_MESSAGE_PID_RUNNING, $pid));

                return false;
            }
            Cron::out(sprintf(self::CRONJOB_MESSAGE_CLEARING_DATABASE_PID, $pid));
            Settings::set($pidkey, 0);
        }
        Settings::set($pidkey, getmypid());

        return true;
    }

    abstract public function process();
}