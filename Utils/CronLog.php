<?php
namespace Tikwork\Utils;
class CronLog
{
    const LOG_TEMPLATE = '[%s] %s';
    private $logFile = null;
    public function __construct($logFile=null)
    {
        $this->logFile = $logFile;
        if(!$this->logFile) {
            $this->logFile = TIKWORK__LOG_DIR.'/cron-log-'.date('Y-m-d',time());
        }

    }
    public function log($message)
    {
        error_log(sprintf(self::LOG_TEMPLATE,date('Y-m-d H:i:s',time()),$message),3,$this->logFile);
    }
}