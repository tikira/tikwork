<?php
namespace Tikwork\Utils;
class CronMail
{
    const SUBJECT_TEMPLATE = '[Cron:%s] %s: Status: %s';

    private $receivers = null;

    public function __construct($receivers, $level = Cron::STATUS_ERROR)
    {
        $this->receivers = $receivers;
    }

    public function send(CronJob $cronjob, $log, $status)
    {
        $sendMail = false;
        if (defined('CRONMAIL__LEVEL') && $status == CRONMAIL__LEVEL) {
            $sendMail = true;
        }

        if ($sendMail) {
            $mail = new HtmlMimeMail5();
            $mail->setSubject(
                sprintf(
                    self::SUBJECT_TEMPLATE,
                    $cronjob->cronName,
                    DateX::convert2GER(time()),
                    $status
                )
            );
            $mail->setText($log);

            try {
                $ok = $mail->send(explode(',', $this->receivers));
            } catch (\Exception $e) {
                throw $e;
            }
        }
    }
}