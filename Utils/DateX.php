<?php
namespace Tikwork\Utils;
class DateX
{
    const DIN = '/^([0-9]{2,4}).([0-9]{1,2}).([0-9]{1,2})(.([012][0-9]).([0-9]{2}).([0-9]{2}))?$/';
    const GER = '/^([0-9]{2}).([0-9]{2}).([0-9]{2,4})(.([012][0-9]).([0-9]{2}).([0-9]{2}))?$/';

    /**
     * Convert a Date or Timestamp into a German Formated Time
     *
     * @param integer $sDate Date or Timestamp
     * @param boolean $bIgnoreTime True/False
     * @param boolean $bOnlyTime true/false
     *
     * @return string
     */
    public static function convert2GER($sDate, $bIgnoreTime = false, $bOnlyTime = false)
    {
        $sDate = self::convert2TS($sDate, $bIgnoreTime);

        if ($bIgnoreTime) {
            return date('d.m.Y', $sDate);
        } elseif ($bOnlyTime) {
            return date('H:i:s', ($sDate));
        } else {
            return date('d.m.Y H:i:s', $sDate);
        }
    }

    /**
     * Converts a Date or Timestamp into a Timestamp
     *
     * @param string $sDate Date or Timestamp
     * @param boolean $bIgnoreTime true/false
     *
     * @return integer
     */
    public static function convert2TS($sDate, $bIgnoreTime = false)
    {
        $aSplit = array();
        if (is_numeric($sDate)) {
            return $sDate;
        }
        $split = explode(' ', $sDate);
        if ($bIgnoreTime) {
            $sDate = $aSplit[0];
        }
        $e = new \DateTime($sDate);

        return $e->getTimestamp();
    }

    /**
     * Convert a Date or timestamp into DIN-Formated Date
     *
     * @param string $sDate string
     * @param boolean $bIgnoreTime true/false
     * @param boolean $bOnlyTime true/false
     *
     * @return string
     */
    public static function convert2DIN($sDate, $bIgnoreTime = false, $bOnlyTime = false)
    {
        $_date = self::convert2TS($sDate, $bIgnoreTime, $bOnlyTime);
        if ($bIgnoreTime) {
            return date('Y-m-d', $sDate);
        } elseif ($bOnlyTime) {
            return date('H:i:s', ($sDate));
        } else {
            return date('Y-m-d H:i:s', $sDate);
        }
    }
}