<?php
namespace Tikwork\Utils;
class Encoding
{
    public function encode($text, $encTarget = 'UTF-8')
    {
        $enc = mb_detect_encoding($text, array('UTF-8', 'ISO-8859-15'));
        if ($enc == 'ISO-8859-15' && preg_match('/[\x80-\x9f]/', $text)) {
            $enc = 'Windows-1252';
        }

        //echo $enc . '(' . urlencode($text) . ')<br>';

        if ($enc == $encTarget) {
            return $text;
        }

        return @mb_convert_encoding($text, $encTarget, $enc);
    }
}
