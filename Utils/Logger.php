<?php
namespace Tikwork\Utils;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;

class Logger
{
    private static $logger = null;

    public static function getLogger()
    {
        if(!self::$logger instanceof \Monolog\Logger) {
            $logger = new \Monolog\Logger(APPLICATION_NAME);

            if (MPDEV) {
                $debugFile = TIKWORK__LOG_DIR . '/debug-' . (new \DateTime())->format('Y-m-d') . '.log';
                try {
                    if(!file_exists($debugFile)) {
                        touch($debugFile);
                        chmod($debugFile,___MCP_PERMISSION_MODE);
                    }
                } catch(\Exception $e) {
                    throw new \Exception('Cannot create debug-logfile '.$debugFile);
                }
                $debugHandler = new StreamHandler($debugFile,\Monolog\Logger::DEBUG);
                $debugHandler->setFormatter(new LineFormatter(null, null, true));
                $logger->pushHandler($debugHandler);
            }
            $errorFile = TIKWORK__LOG_DIR . '/error-' . (new \DateTime())->format('Y-m-d') . '.log';
            try {
                if(!file_exists($errorFile)) {
                    touch($errorFile);
                    chmod($errorFile,___MCP_PERMISSION_MODE);
                }
            } catch(\Exception $e) {
                throw new \Exception('Cannot create error-logfile '.$errorFile);
            }
            $errorHandler = new StreamHandler($errorFile, \Monolog\Logger::ERROR);
            $logger->pushHandler($errorHandler);

            if (php_sapi_name() == 'cli' && !defined('UNITTEST')) {
                $consoleLog       = new StreamHandler('php://stdout', \Monolog\Logger::INFO);
                $consoleFormatter = new LineFormatter("%message%\n");
                $consoleFormatter->allowInlineLineBreaks(true);
                $consoleLog->setFormatter($consoleFormatter);
                $logger->pushHandler($consoleLog);
            }
            self::$logger = $logger;
        }

        return self::$logger;
    }

    public static function setLogger(\Monolog\Logger $logger) {
        self::$logger = $logger;
    }
}
