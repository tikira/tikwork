<?php

class RequestField
{
    public $required;
    public $allowed;
    public $length;
    public $default;

    /**
     * @param bool $required
     * @param null|string $allowed
     * @param int|array $length
     * @param null|string $default
     */
    public function __construct($required = true, $allowed = null, $length = null, $default = null)
    {
        $this->required = $required;
        $this->allowed  = $allowed;
        $this->default  = $default;
        if (is_string($length)) {
            $length = array(
                'minimal' => $length,
                'maximal' => $length
            );
        }
        $this->length = (object)$length;
    }

    /**
     * @param bool $required
     * @param null|string $allowed
     * @param null|int $minLength
     * @param null|int $maxLength
     * @param null|string $default
     *
     * @return RequestField
     */
    public static function factory($required = true, $allowed = null, $minLength = null, $maxLength = null, $default = null)
    {
        return new self(
            $required,
            $allowed,
            array(
                'minimal' => $minLength,
                'maximal' => $maxLength
            ),
            $default
        );
    }

    public function validate($value)
    {
        if ($this->required === true && !$value) {
            return false;
        }
        if ($value) {
            if ($this->allowed != null && !preg_match($this->allowed, $value)) {
                return false;
            }
            if ($this->length->minimal > 0 && strlen($value) < $this->length->minimal) {
                return false;
            }
            if ($this->length->maximal > 0 && strlen($value) > $this->length->maximal) {
                return false;
            }
        } else {
        }

        return true;
    }
}