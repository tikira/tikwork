<?php
/**
 * Sys Class
 *
 * PHP version 5
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */

/**
 * Sys Class
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */
namespace Tikwork\Utils;

class SYS
{
    public static
        $consoleColors = array(
        0   => 0,       // COLOR_RESET
        1   => 30,      // COLOR_BLACK
        2   => 31,      // COLOR_RED
        4   => 32,      // COLOR_GREEN
        8   => 33,      // COLOR_YELLOW
        16  => 34,      // COLOR_BLUE
        32  => 35,      // COLOR_PURPLE
        64  => 36,      // COLOR_CYAN
        128 => 37       // COLOR_GREY
    );

    /**
     * Styles
     *
     */
    public static
        $consoleStyles = array(
        0    => 0,     // STYLE_NORMAL
        256  => 1,     // STYLE_BOLD
        512  => 4,     // STYLE_UNDERSCORE
        1024 => 5,     // STYLE_BLINK
        2048 => 7,     // STYLE_REVERSE
        4096 => 8      // STYLE_CONCEALED
    );

    const COLOR_RESET  = 0;
    const COLOR_BLACK  = 1;
    const COLOR_RED    = 2;
    const COLOR_GREEN  = 4;
    const COLOR_YELLOW = 8;
    const COLOR_BLUE   = 16;
    const COLOR_PURPLE = 32;
    const COLOR_CYAN   = 64;
    const COLOR_GREY   = 128;
    const COLOR_VALID  = 255;


    const STYLE_NORMAL     = 0;
    const STYLE_BOLD       = 256;
    const STYLE_UNDERSCORE = 512;
    const STYLE_BLINK      = 1024;
    const STYLE_REVERSE    = 2048;
    const STYLE_CONCEALED  = 4096;
    const STYLE_VALID      = 7936;

    /**
     * Dumps a Object in Human-Readable Format (HTML)
     *
     * @param Object $_a Object to Dump
     * @param Boolean $return Return true/false
     *
     * @return string
     */
    public static
    function out($_a, $_return = false)
    {
        if (!is_array($_a) && !is_object($_a)) {
            $_a = array($_a);
        }
        $sContent = '<div style="background-color: black; width: 100%;margin:0px;color:white;">';
        $sContent .= self::getHeader();
        $sContent .= SYS::dumpArray($_a);
        $sContent .= '</div></div>';
        if ($_return) {
            return $sContent;
        } else {
            echo $sContent;
        }
    }

    /**
     * Prints the Headers
     *
     * @return void
     */
    public static
    function getHeader()
    {
        $trace    = debug_backtrace();
        $filename = basename($trace[1]['file']);
        $line     = $trace[1]['line'];

        if (isset($trace[2])) {
            if ($trace[2]['function']) {
                $function = $trace[2]['function'];
            } else {
                $function = '-';
            }
            if (isset($trace[2]['class'])) {
                $class = $trace[2]['class'];
            } else {
                $class = '-';
            }
        } else {
            $function = '-';
            $class    = '-';
        }
        $result = '<table border="0" style="margin: 0px 5px;">';
        $result .= '<tr><th style="font-size:10px;font-family:verdana;color:white;">Datei:</th>';
        $result .= '<th style="font-size:10px;font-family:verdana;color:#cddae5;font-weight:bold;">';
        $result .= $filename . '#' . $line . '</th>';
        $result .= '<th style="font-size:10px;font-family:verdana;color:white;">Logic:</th>';
        $result .= '<th style="font-size:10px;font-family:verdana;color:#cddae5;font-weight:bold;">';
        $result .= $class . '::' . $function . '</th></tr>';
        $result .= '</table>';

        return $result;
    }

    /**
     * get a Name of the given Class
     *
     * @param object $o Object
     *
     * @return string
     */
    public static
    function getClassName($o)
    {
        $ro = new ReflectionObject($o);

        return $ro->getName();
    }

    /**
     * Returns the Type of given object
     *
     * @param object $a Object
     *
     * @return string
     */
    public static
    function getType($a)
    {
        $type = '';
        if (is_bool($a)) {
            $type = 'boolean';
            $a    = (is_bool($a)) ? 'true' : 'false';
        } elseif ($type == '' && is_string($a)) {
            $type = 'string';
        } elseif ($type == '' && is_array($a)) {
            $type = 'array';
            $a    = '[count: ' . count($a) . ']';
        } elseif ($type == '' && is_object($a)) {
            $type = 'object';
            $a    = '[object: ' . self::getClassName($a) . ']';
        } elseif ($type == '' && is_float($a)) {
            $type = 'float';
        } elseif ($type == '' && is_numeric($a)) {
            $type = 'integer';
        } else {
            $type = 'unknown';
        }

        return $type;
    }

    /**
     * Dumps a Array
     *
     * @param array $_a Array
     *
     * @return void
     */
    public static
    function dumpArray($_a)
    {
        $sContent = '<table border=\'0\' style=\'border: 1px solid red; margin: 5px;\'>';
        foreach ($_a as $k => $v) {
            $type = SYS::getType($v);
            if (is_array($v)) {
                $sContent .= '<tr><td style=\'color:white;\'>' . $k;
                $sContent .= '</td><td style=\'font-family:verdana;font-size:10px;color:#cddae5;\'>';
                $sContent .= $type . '</td><td>';
                $sContent .= SYS::dumpArray($v);
                $sContent .= '</td></tr>';
            } elseif (is_object($v)) {
                $sContent .= '<tr><td style=\'color:white;\'><pre>' . var_export($v, true) . '</pre></td></tr>';
            } else {
                $v = is_bool($v) ? $v ? 'true' : 'false' : $v;
                $sContent .= '<tr><td style="color:red;font-family:verdana;font-size:10px;">' . $k . '</td>';
                $sContent .= '<td style="font-family:verdana;font-size:10px;color:#cddae5;">' . $type . '</td>';
                $sContent .= '<td style="color:red;font-family:verdana;font-size:10px;">' . $v . '</td></tr>';
            }
        }
        $sContent .= '</table>';

        return $sContent;
    }

    /**
     * Prints a String to the Console with Colors and Styles
     *
     * @param string $msg Message to Show
     * @param string $color Color
     * @param string $style Style
     *
     * @return void
     */
    public static
    function consoleOut($msg, $color = 0, $style = 0)
    {
        if (($color = ($color & self::COLOR_VALID)) && isset(SYS::$consoleColors[$color])) {
            $color = SYS::$consoleColors[$color];
        } else {
            $color = '';
        }

        if (($style = ($style & self::STYLE_VALID)) && isset(SYS::$consoleStyles[$style])) {
            $style = SYS::$consoleStyles[$style];
        } else {
            $style = 0;
        }

        $retval = sprintf("\033[%s;%sm ", $style, $color);
        $retval .= $msg . " \033[0;m" . PHP_EOL;
        echo $retval;

    }

}

?>
