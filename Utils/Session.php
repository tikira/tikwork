<?php
/**
 * Session Class
 *
 * PHP version 5
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */

/**
 * Session Class
 *
 * @package Framework
 * @author  Sascha Kilian <untoter2000@hotmail.com>
 *
 */
namespace Tikwork\Utils;
class Session
{
    /**
     * @var string Sessionprefix
     */
    public static  $prefix;
    /** @var \PDO */
    private static $pdo;

    /**
     * Starts a Session
     *
     * @param \PDO $pdo If set to an valid PDO Object, we use DB to save the Sessions
     * @return void
     */
    public static function start(\PDO $pdo = null)
    {
        if ($pdo) {
            self::register($pdo);
        }
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (defined('SESSION_PREFIX') && SESSION_PREFIX != '') {
            self::$prefix = SESSION_PREFIX;
        }
    }

    public static function register($pdo)
    {
        self::$pdo = $pdo;
        session_set_save_handler(
            array('Session', 'open'),
            array('Session', 'close'),
            array('Session', 'read'),
            array('Session', 'write'),
            array('Session', 'deleteSession'),
            array('Session', 'gc')
        );
    }

    /**
     * Sets a Session Value by Key
     *
     * @param string $_key Key for Value
     * @param string $_value Value for Key
     * @param boolean $usePrefix Use Session Prefix
     *
     * @return void
     */
    public static function set($_key, $_value, $usePrefix = true)
    {
        if ($usePrefix && self::$prefix) {
            if (!isset($_SESSION[self::$prefix])) {
                $_SESSION[self::$prefix] = array();
            }
            $_SESSION[self::$prefix][$_key] = $_value;
        } else {
            $_SESSION[$_key] = $_value;
        }

    }

    /**
     * Get a Session value by Key
     *
     * @param string $_key Key for Value
     * @param boolean $usePrefix Use Sessionprefix
     *
     * @return mixed
     */
    public static function get($_key = null, $usePrefix = true)
    {
        if (!$_key) {
            if (self::$prefix && $usePrefix) {
                if (isset($_SESSION[self::$prefix])) {
                    return $_SESSION[self::$prefix];
                }

                return null;
            } else {
                return $_SESSION;
            }
        } else {
            if (self::$prefix && $usePrefix) {
                if (isset($_SESSION[self::$prefix]) && isset($_SESSION[self::$prefix][$_key])) {
                    return $_SESSION[self::$prefix][$_key];
                }
            } else {
                if (isset($_SESSION[$_key])) {
                    return $_SESSION[$_key];
                }
            }

        }

        return null;
    }

    /**
     * Destroys the Session
     *
     * @return void
     */
    public static function destroy()
    {
        session_destroy();
    }

    /**
     * Dumps the Session
     *
     * @return void
     */
    public static function dump()
    {
        print_r($_SESSION);
    }

    /**
     * Delete a Key for this Session
     *
     * @param string $_key Key for Delete
     *
     * @return void
     */
    public static function delete($_key)
    {
        if (isset($_SESSION[$_key])) {
            unset($_SESSION[$_key]);
        }
    }

    public static
    function open($path, $session_id)
    {
        return true;
    }

    public static
    function close()
    {
        session_write_close();

        return true;
    }

    public static
    function read($session_id)
    {
        $q    = 'SELECT `data` FROM `sessions` WHERE `id` = :id';
        $stmt = Session::$pdo->prepare($q);
        $stmt->bindValue('id', $session_id);
        $result = $stmt->execute();
        $data   = $stmt->fetchObject();
        if ($data && $data->data) {
            return $data->data;
        }

        return null;
    }

    public static
    function write($session_id, $data)
    {
        $q    = 'INSERT INTO `sessions`
            (`id`,`data`,`created`,`modified`,`ip_address`)
            VALUES 
            (:id,:data,NOW(),null,:ip_address)
            ON DUPLICATE KEY UPDATE
              `data` = :data,
              `modified` = NOW()
        ';
        $stmt = self::$pdo->prepare($q);
        $stmt->bindParam('id', $session_id);
        $stmt->bindParam('data', $data);
        $stmt->bindParam('ip_address', $_SERVER['REMOTE_ADDR']);

        return $stmt->execute();
    }

    public static
    function deleteSession($session_id)
    {
        $q    = 'UPDATE `sessions` SET `id` = concat(:id,"-deleted") WHERE id = :id';
        $stmt = self::$pdo->prepare($q);
        $stmt->bindParam('id', $session_id);

        return $stmt->execute();
    }


    public static
    function gc($lifetime)
    {
        return true;
    }
}