<?php
namespace Tikwork\Utils;

use Tikwork\System\Handlers\SettingsDatabase;
use Tikwork\System\Handlers\SettingsFile;
use Tikwork\System\Handlers\SettingsHandlerAbstract;

class Settings
{
    private static $instance = null;
    private        $handler  = null;

    const TYPE_MYSQL = 'Database';
    const TYPE_FILE  = 'File';

    public function __construct($type, $params = null, SettingsHandlerAbstract $handler = null)
    {
        if (!$handler) {
            switch ($type) {
                case self::TYPE_FILE:
                    if ($params) {
                        $handler = new SettingsFile();
                    }
                    break;
                case self::TYPE_MYSQL:
                    $handler = new SettingsDatabase();
                    break;
                default:
                    throw new \Exception('Cant find a Handler for your selected Settings Type');
                    break;
            }
        }
        if ($handler) {
            $handler->prepare($params);
            $this->handler = $handler;
            self::registerInstance($this);
        }
    }

    public static function init($type, $params = null, SettingsHandlerAbstract $handler = null)
    {
        return new Settings($type, $params, $handler);
    }

    public static function registerInstance(Settings $instance)
    {
        self::$instance = $instance;
    }

    public static function get($name)
    {
        if (self::$instance instanceof Settings) {
            return self::$instance->handler->get($name);
        } else {
            return null;
        }

    }

    public static function set($name, $value)
    {
        if (self::$instance instanceof Settings) {
            return self::$instance->handler->set($name, $value);
        } else {
            return null;
        }
    }
}
