<?php
namespace Tikwork\Utils;

use Tikwork\Database\DBCon;

class Url
{
    const PARAMETER_SEPARATOR = '.';
    const FIRST_SEPARATOR     = '(';
    const SECOND_SEPARATOR    = ')';

    public        $fieldArray       = array();
    public static $staticFieldArray = array();

    public
    function __construct($url)
    {
        $this->fieldArray[] = Url::getRewrite();
        $str                = explode('/', $url);

        $str = $str[(count($str) - 1)];

        $split   = explode('?', $str);
        $rewrite = str_replace('.html', '', $split[0]);
        $params  = explode(self::PARAMETER_SEPARATOR, str_replace(array('(', ')'), '', $split[1]));

        $this->fieldArray[] = $rewrite;
        $this->fieldArray   = array_merge($this->fieldArray, $params);

        return $this;
    }

    /**
     * get a Copy from the Parsed Url
     *
     * @return string
     */
    public static
    function getCopy()
    {
        return 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    /**
     * Makes a String (Url) from this Object
     *
     * @throws Exception
     *
     * @return string
     */
    public
    function __toString()
    {
        $result = null;
        $result = eAModule::getRewriteById($this->fieldArray[0]) . '.html';

        $params = null;
        $cnt    = count($this->fieldArray);
        if ($cnt > 1) {
            for ($i = 1; $i < $cnt; $i++) {
                $params[$i] = $this->fieldArray[$i];
            }
        }

        $p1 = null;
        if ($params) {
            $p1 = implode(self::PARAMETER_SEPARATOR, $params);
        }

        $paramsString = '?' . self::FIRST_SEPARATOR . $p1 . self::SECOND_SEPARATOR;
        if ($paramsString != '?' . self::FIRST_SEPARATOR . self::SECOND_SEPARATOR) {
            $result .= $paramsString;
        }

        return $result;
    }

    /**
     * makes a Url for given Parameters
     * 1 Parameter is a ModuleId
     * second and above Parameters are Url parameters
     *
     * @throws Exception
     *
     * @return string
     */
    public static
    function make()
    {
        $_params = func_get_args();

        if (isset($_params[0])) {
            $page = $_params[0];
        } else {
            return false;
        }
        unset($_params[0]);
        if (isset($_params[1]) && is_array($_params[1])) {
            $_params = $_params[1];
        }
        $result = null;

        $mod = (object)DBCon::getSingleRow('SELECT * FROM module WHERE name = :name OR id = :name OR rewrite_url = :name', array('name' => $page));

        if ($mod->id < 0) {
            return null;
        }
        if ($mod->rewrite_url) {
            $result = $mod->rewrite_url . ".html";
        } else {
            $result = $mod->id . '.html';
        }

        $params = null;
        if ($_params) {
            foreach ($_params as $value) {
                $params[] = $value;
            }
        }

        $p1 = null;
        if ($params) {
            $p1 = implode(self::PARAMETER_SEPARATOR, $params);
        }

        $paramsString = '?' . self::FIRST_SEPARATOR . $p1 . self::SECOND_SEPARATOR;
        if ($paramsString != '?' . self::FIRST_SEPARATOR . self::SECOND_SEPARATOR) {
            $result .= $paramsString;
        }

        return $result;
    }

    public static function makeFromArray($_params)
    {
        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '?' . http_build_query($_params);
    }


    /**
     * Get all Parameters
     *
     * @return boolean
     */
    public static
    function getParams()
    {
        Url::$staticFieldArray[] = Url::getRewrite();
        if (isset($_GET['params'])) {
            $value = $_GET['params'];
            $value = str_replace(self::FIRST_SEPARATOR, '', $value);
            $value = str_replace(self::SECOND_SEPARATOR, '', $value);
            $value = explode(self::PARAMETER_SEPARATOR, $value);
            foreach ($value as $key) {
                Url::$staticFieldArray[] = $key;
            }
        }

        return true;
    }

    /**
     * Clears a Url
     *
     * @return boolean
     */
    public static
    function clear()
    {
        Url::$staticFieldArray    = array();
        Url::$staticFieldArray[0] = Url::getRewrite();

        return true;
    }

    /**
     * Set a Parameter by Index
     *
     * @param integer $index Index for this Parameter
     * @param string $value Value for this Parameter
     *
     * @return void
     */
    public static
    function set($index, $value)
    {
        Url::$staticFieldArray[$index] = $value;
    }

    /**
     * Get Rewrite name for Module
     *
     * @return int
     */
    public static
    function getRewrite()
    {
        $result = null;
        if (isset($_GET['rewrite']) && strlen($_GET['rewrite']) > 0) {
            $o = new eAModule();
            $o->readWhere('status > 0 AND rewrite_url = :rewrite', array('rewrite' => $_GET['rewrite']));
            $result = $o->id;
        }

        return $result;
    }

    /**
     * Get Parsed Url Object
     *
     * @param string $url URL
     *
     * @return Url
     */
    public static
    function getParsedUrl($url)
    {
        return new Url($url);
    }

    /**
     * Get Url for a Module
     *
     * @param integer $_id Moduleid
     *
     * @return string
     */
    public static
    function getUrlForModuleId($_id)
    {
        $eModul = new eAModule();
        $eModul->read($_id);

        return Url::make($eModul->id);
    }

    public static function createUrl($controller, $action = 'index', $params = null)
    {
        $url = sprintf('%s://%s/%s/%s',
            ((isset($_SERVER['REQUEST_SCHEME'])) ? $_SERVER['REQUEST_SCHEME'] : CLI),
            ((isset($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : 'localhost'),
            $controller,
            $action);
        if (is_array($params)) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }

    /**
     * Gets a value from url by given index
     *
     * @param integer $_index Index in Url
     *
     * @return string
     */
    public static function get($key)
    {
        if (count(Url::$staticFieldArray) == 0) {
            Url::getParams();
        }
        if (isset(Url::$staticFieldArray[$key])) {
            return Url::$staticFieldArray[$key];
        }

        if (isset($_GET[$key])) {
            return $_GET[$key];
        }

        return null;
    }

    public static function getControllerName($default = 'Portal')
    {
        return self::getUrlPart(1, $default);

    }

    public static function getPackageName($default = 'Portal')
    {
        return self::getUrlPart(0, $default);
    }

    private static function getUrlPart($idx, $default = null)
    {
        $url = null;
        if (isset($_GET['q'])) {
            $url = $_GET['q'];
        }
        if (!$url) {
            return $default;
        }
        $urlParts = explode('/', $url);
        if (isset($urlParts[$idx]) && $urlParts[$idx]) {
            return $urlParts[$idx];
        } else {
            return $default;
        }
    }

    public static function getActionName($default = 'index')
    {
        return self::getUrlPart(2, $default);
    }
}