<?php

namespace Tikwork\Utils;

class UrlParameter
{
    const MODE_GET     = 'GET';
    const MODE_POST    = 'POST';
    const MODE_REQUEST = 'REQUEST';
    const MODE_FILES   = 'FILES';
    const MODE_COOKIE  = 'COOKIE';
    const MODE_ENV     = 'ENV';
    const MODE_SERVER  = 'SERVER';
    const MODE_ALL     = 'ALL';

    private static $defaultMode = null;
    private static $params      = [];

    public static function init($defaultMode = self::MODE_GET)
    {
        self::$params['GET']     = $_GET;
        self::$params['POST']    = $_POST;
        self::$params['REQUEST'] = $_REQUEST;
        self::$params['FILES']   = $_FILES;
        self::$params['COOKIE']  = $_COOKIE;
        self::$params['ENV']     = $_ENV;
        self::$params['SERVER']  = $_SERVER;
        self::$defaultMode       = $defaultMode;
    }

    public static function setMode($mode)
    {
        self::$defaultMode = $mode;
    }

    public static function get($name, $mode = self::MODE_REQUEST, $allowed = null)
    {
        if (!isset(self::$params[$mode]) || !isset(self::$params[$mode][$name])) {
            return false;
        }
        if ($allowed && !preg_match($allowed, self::$params[$mode][$name])) {
            return false;
        }

        return self::$params[$mode][$name];
    }
}