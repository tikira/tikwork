<?php
namespace Tikwork\Viewport;
use Tikwork\Interfaces\iTemplateEngine;

class Block
{
    const POSITION_AFTER = '-1';
    const POSITION_BEFOR = '1';

    private $blockname = null;
    private $template  = null;
    private $childs    = [];
    private $data      = [];
    private $engine    = null;

    public function __construct($name,$template=null,iTemplateEngine $engine=null)
    {
        $this->blockname = $name;
        $this->template = ($template) ? $template : $this->template;
        $this->engine = $engine;
    }

    public function setTemplateEngine(iTemplateEngine $engine)
    {
        $this->engine = $engine;
    }

    public function addChild(Block &$block,$position=self::POSITION_AFTER)
    {
        if($position == self::POSITION_AFTER) {
            $this->childs[] = $block;
        } elseif($position == self::POSITION_BEFOR) {
            $this->childs = array_merge([$block],$this->childs);
        }
    }

    public function __toString()
    {
        $result = '';
        $debug = true;
        if($debug) {
            $result = '['.$this->blockname.']';
        }
        $result .= $this->render();
        if($this->childs && count($this->childs)>0) {
            foreach($this->childs as $child) {
                $result .= $child->__toString();
            }
        }
        return $result;
    }

    public function assign($var,$value=null)
    {
        $this->data[$var] = $value;
    }

    public function render()
    {
        if($this->engine instanceof iTemplateEngine && $this->template) {
            return $this->engine->fetch($this->template);
        } else {
            return 'no content';
        }
    }

    public function getChild($name)
    {
        if(isset($this->childs[$name])) {
            return $this->childs[$name];
        }
        return null;
    }
}