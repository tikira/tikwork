<?php
namespace Tikwork\Viewport;

class Viewport extends Block
{
    public function __construct($name,$template=null)
    {
        $smarty = new \ExtSmarty();
        parent::__construct($name, $template);
        $html = new Block('html');
        $html->setTemplateEngine($smarty);
        $head = new Block('head');
        $body = new Block('body');
        $content = new Block('content');
        $body->addChild($content);
        $html->addChild($head);
        $html->addChild($body);
        $this->addChild($html);

        return $this;
    }
}