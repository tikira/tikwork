<?php
if(!defined('TIKWORK_DIR')) {
    define('TIKWORK_DIR',__DIR__.'/');
}
if(!defined('BASE_DIR')) {
    define('BASE_DIR', TIKWORK_DIR . '../../');
}
if(!defined('APPLICATION_NAMESPACE')) {
    define('APPLICATION_NAMESPACE','tikwork');
}
if(!defined('APPLICATION_MODUL')) {
    throw new Exception('need const APPLICATION_MODUL to work with tikwork');
}
if(!defined('CONFIG_DIR')) {
    define('CONFIG_DIR', BASE_DIR . 'etc/');
}
if(!defined('TIKWORK__TEMP_DIR')) {
    define('TIKWORK__TEMP_DIR', ___MCP_VAR . 'tmp/'.APPLICATION_MODUL.'/'.APPLICATION_NAMESPACE.'/');
}
if(!defined('TIKWORK__LOG_DIR')) {
    define('TIKWORK__LOG_DIR', ___MCP_VAR . 'logs/'.APPLICATION_MODUL.'/'.APPLICATION_NAMESPACE.'/');
}
if(!defined('TIKWORK__DATA_DIR')) {
    define('TIKWORK__DATA_DIR', TIKWORK__TEMP_DIR.'/data/');
}

require_once(TIKWORK_DIR . 'init.php');