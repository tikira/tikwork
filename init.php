<?php namespace Tikwork
{
    /**
     * Basic Initialisation for Framework
     *
     * PHP version 5
     *
     * @package Framework
     * @author  Sascha Kilian <sascha.kilian83@googlemail.com>
     *
     */
    require_once(__DIR__ . '/System/Tikwork.php');
    require_once(__DIR__ . '/System/Event.php');

    if (!defined('APPLICATION_NAME')) {
        define('APPLICATION_NAME','default');
    }

    if (!defined('TIKWORK__DEBUG')) {
        define('TIKWORK__DEBUG', false);
    }
    if (!defined('TIKWORK__ERRORHANDLER')) {
        define('TIKWORK__ERRORHANDLER', true);
    }
    if (!defined('TIKWORK__LOG_DIR')) {
        throw new \Exception('no TIKWORK__LOG_DIR defined');
    }
    if (!defined('TIKWORK__TEMP_DIR')) {
        throw new \Exception('no TIKWORK__TEMP_DIR defined');
    }
    if (!defined('TIKWORK__DATA_DIR')) {
        throw new \Exception('no TIKWORK__DATA_DIR defined');
    }
    System\Tikwork::init();

    try {
        if (!is_dir(TIKWORK__TEMP_DIR)) {

            $ok = @mkdir(TIKWORK__TEMP_DIR, ___MCP_PERMISSION_MODE, true);
            if (!$ok) {
                error_log('cant create temp_dir ' . TIKWORK__TEMP_DIR);
                exit();
            }

        }
        if (!is_dir(TIKWORK__LOG_DIR)) {
            $ok = @mkdir(TIKWORK__LOG_DIR, ___MCP_PERMISSION_MODE, true);
            if (!$ok) {
                error_log('cant create log_dir ' . TIKWORK__LOG_DIR);
                exit();
            }
        }
        if (!is_dir(TIKWORK__DATA_DIR)) {
            $ok = @mkdir(TIKWORK__DATA_DIR, ___MCP_PERMISSION_MODE, true);
            if (!$ok) {
                error_log('cant create data_dir ' . TIKWORK__DATA_DIR);
                exit();
            }
        }
    } catch (\Exception $e) {
        error_log($e->getMessage());
    }
}
namespace
{

    use Tikwork\Utils\Logger;

    /**
     * @return \Monolog\Logger
     */
    function getLogger()
    {
        return Logger::getLogger();
    }

}