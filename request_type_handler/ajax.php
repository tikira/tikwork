<?php
require_once(__DIR__ . '/../init.inc.php');
header('Content-Type: text/html; charset=UTF-8');
error_reporting(E_ALL);
ini_set('display_errors', 1);

class ajax
{
    private $params = null;
    private $class  = null;

    public
    function __construct()
    {

    }

    public
    function process()
    {
        $params = (object)array_merge($_GET, $_POST);
        $_POST  = $_GET = null;

        if (!isset($params->action)) {
            $this->sendMessage('Invalid Action', false);
        }

        $action = $params->action;

        $actionFile = BASE_DIR . '/Actions/AJAX/' . $action . '.php';

        if (file_exists($actionFile)) {
            try {
                $class = include $actionFile;
            } catch (Exception $e) {
                $this->sendMessage('Action: "' . $params->action . '" No Valid Action', false);
            }

        } else {
            $this->sendMessage('Action: "' . $params->action . '" is not Valid', false);
        }

        if (isset($class->loginRequired) && $class->loginRequired === true) {
            if (Session::get('user') <= 0) {
                $this->sendMessage('Auth Required!', false);
            }
        }

        $this->class = $class;
        $this->parseParams($params);
        if (@$this->class->getOriginalParams && $this->class->getOriginalParams === true) {
            $this->params->__original_params = $params;
        }
        $this->sendData($class->process($this->params));

    }

    private
    function parseParams($params)
    {
        $newParams = array();
        if (isset($this->class->members)) {
            foreach ($this->class->members as $key => $options) {
                $options = (object)$options;

                if (
                    property_exists($options, 'required') &&
                    $options->required == true &&
                    !property_exists($params, $key)
                ) {
                    $this->sendMessage('Missing Required Parameter: ' . $key, false);
                }

                if (
                    property_exists($options, 'allowed') &&
                    $options->allowed != null &&
                    property_exists($params, $key) &&
                    !preg_match($options->allowed, $params->$key)
                ) {
                    $this->sendMessage('Parameter ' . $key . ' has not a Valid Content!', false);
                }
                if (property_exists($params, $key)) {
                    $newParams[$key] = $params->$key;
                }
            }
            $this->params = (object)$newParams;
        }
    }

    private
    function sendMessage($msg, $success = true)
    {
        $result = array(
            'success' => $success,
            'message' => $msg
        );
        $this->sendData($result);
    }

    private
    function sendData($_data)
    {
        echo json_encode($_data);
    }
}

$request = new ajax();
$request->process();
?>