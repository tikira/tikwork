<?php
require_once(__DIR__ . '/../init.inc.php');
header('Cache-Control: no-cache');
header('Content-Type: application/json; charset=UTF-8');
if (GZIP_ENABLE) {
    header('Content-Encoding: gzip');
    ob_start('ob_gzhandler');
}

class ajax
{
    private $params = null;
    private $class  = null;

    public
    function __construct()
    {
        $this->params = (object)$_REQUEST;
        if ($_SERVER && isset($_SERVER['PATH_INFO'])) {
            $this->params->action = $_SERVER['PATH_INFO'];
        } elseif ($_GET && isset($_GET['action'])) {
            $this->params->action = $_GET['action'];
        } elseif ($_POST && isset($_POST['action'])) {
            $this->params->action = $_POST['action'];
        }
    }

    public
    function process()
    {
        if (empty($this->params->action)) {
            return $this->sendMessage('Missing Parameter: action', false);
        }
        $action     = $this->params->action;
        $actionFile = BASE_DIR . '/Actions/AJAX/' . $action . '.php';

        if (file_exists($actionFile)) {
            try {
                $class = include_once($actionFile);
            } catch (Exception $e) {
                return $this->sendMessage('Action: "' . $$this->params->action . '" throws a Exception', false);
            }

        } else {
            return $this->sendMessage('Action: "' . $this->params->action . '" is not Valid', false);
        }

        if (property_exists($class, 'loginRequired') && $class->loginRequired === true) {
            if (Session::get(ApplicationLogic::SESSION_IDX_USER) <= 1) {
                return $this->sendMessage('Login Required for Action!', false);
            }
        }

        if (isset($class->acl) && $class->acl > 0) {
            if (Session::get(ApplicationLogic::SESSION_IDX_ACL) < $class->acl) {
                return $this->sendMessage('Permission Denied!', false);
            }
        }


        $class;
        if (!property_exists($class, 'inputValidation') || $class->inputValidation === true) {
            $this->validateParams($class, $this->params);
        }

        $data = $class->process($this->params);

        return $this->sendData($data);
    }

    private
    function validateParams($modul, $params)
    {

        $newParams = array();
        if (method_exists($modul, 'getMembers')) {
            $members = $modul->getMembers();
        } else {
            $members = null;
        }

        if (is_array($members)) {
            foreach ($members as $key => $field) {
                if (
                    $field->required == true &&
                    !property_exists($params, $key)
                ) {
                    $this->sendMessage('Missing Required Parameter: ' . $key, false);
                }

                if (
                    $field->allowed !== null &&
                    property_exists($params, $key) &&
                    !preg_match($field->allowed, $params->$key)
                ) {
                    $this->sendMessage('Parameter "' . $key . '" is Invalid!', false);
                }

                if (property_exists($params, $key)) {
                    $newParams[$key] = $params->$key;
                } elseif (
                    property_exists($field, 'default') &&
                    !property_exists($params, $key)
                ) {
                    $newParams[$key] = $field->default;
                }
            }
            $this->params = (object)$newParams;
        }
    }

    private
    function sendMessage($msg, $success = true)
    {
        $result = array(
            'success' => $success,
            'message' => $msg
        );

        return $this->sendData($result);
    }

    private
    function sendData($_data)
    {
        $_data = $this->convertToUtf8($_data);
        echo json_encode($_data);
        ob_flush();

        return true;
    }

    private
    function convertToUtf8($_data)
    {
        $data = mb_convert_variables('UTF-8', 'auto', $_data);

        return $_data;

    }
}

$request = new ajax();


$request->process();

?>