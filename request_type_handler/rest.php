<?php
require_once('../init.inc.php');
header('Cache-Control: no-cache');
header('Content-Type: application/json; charset=UTF-8');
if (GZIP_ENABLE) {
    header('Content-Encoding: gzip');
    ob_start('ob_gzhandler');
}

class REST
{
    private $request  = null;
    private $response = null;

    public static function process()
    {
        $instance = new self();
        $instance->processRequest();
    }

    private function __construct()
    {
        $this->request  = new RESTRequest(array('restful' => true));
        $this->response = new RESTResponse();
    }

    private function processRequest()
    {
        //echo $this->request->controller.':'.(($this->request->action)?$this->request->action:'-').':'.(($this->request->id)?$this->request->id:'-').PHP_EOL;
        $method     = strtolower($this->request->method);
        $controller = $this->getController($this->request->controller);
        $acl        = $this->checkAcl($controller);
        if (!$acl) {
            $this->response->errors[] = 'Permission Denied to Controller';
            $this->sendResponse();
        }
        if ($method != 'get') {
            $this->validateRequest($controller);
        }
        if (count($this->response->errors) > 0) {
            return $this->sendResponse();
        }

        if (method_exists($controller, $method)) {
            $controller->$method($this->request, $this->response);
            if (count($this->response->errors) > 0) {
                $this->response->success = false;
            } else {
                $this->response->success = true;
            }
        } else {
            $this->response->errors[] = 'Controller ' . $this->request->controller . ' has no method to handle ' . $method . ' request';
        }
        $this->sendResponse();
    }

    private function checkAcl($controller)
    {
        if (property_exists($controller, 'loginRequired') && $controller->loginRequired == true) {
            if (Session::get(ApplicationLogic::SESSION_IDX_USER) <= 1) {
                return false;
            }
        }
        if (property_exists($controller, 'acl') && $controller->acl > 0) {
            if (Session::get(ApplicationLogic::SESSION_IDX_ACL) && Session::get(ApplicationLogic::SESSION_IDX_ACL) < $controller->acl) {
                return false;
            }
        }

        return true;
    }

    private function validateRequest($controller)
    {
        $members = $controller->getRESTMembers();
        $params  = $this->request->params;

        if (is_array($members)) {
            foreach ($members as $key => $options) {
                $options = (object)$options;


                if (
                    property_exists($options, 'required') &&
                    $options->required == true &&
                    !property_exists($params, $key)
                ) {
                    $this->response->errors[] = 'Missing Required Parameter: ' . $key;
                    continue;
                }

                if (
                    property_exists($options, 'allowed') &&
                    $options->allowed != null &&
                    property_exists($params, $key) &&
                    !preg_match($options->allowed, $params->$key)
                ) {
                    $this->response->errors[] = 'Missing Required Parameter: ' . $key;
                    continue;
                }
            }
        }
    }

    private function getController($controller)
    {
        $controllerFile = ACTION_DIR . '/REST/' . $controller . '.php';
        if (file_exists($controllerFile)) {
            $ctrl = include($controllerFile);

            return $ctrl;
        } else {
            $this->response->errors[] = 'Cant Load Controller ' . $controller;
            $this->response->message  = 'Exception, cant load Controller ' . $controller;
            $this->sendResponse();
        }
    }

    private function sendResponse()
    {
        echo $this->response->to_json();
        ob_flush();
        exit();
    }
}

REST::process();
